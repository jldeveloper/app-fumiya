package br.com.gabrieloliveira.saleopenerp.lib;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.support.v7.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.gabrieloliveira.saleopenerp.SaleOpenerp;

/**
 * Created by Gabriel on 26/01/2016.
 */
public class Draw {
    public static void addViewInGrid(GridLayout grid, View view, int row, int col) {
        grid.addView(view, new GridLayout.LayoutParams(GridLayout.spec(row, GridLayout.FILL), GridLayout.spec(col, GridLayout.FILL)));
    }

    public static void addViewInGrid(GridLayout grid, View view, GridLayout.LayoutParams lp) {
        grid.addView(view, lp);
    }

    public static GridLayout.LayoutParams gridLayoutParamsColSpan(int row, int colstart, int colspan) {
        GridLayout.LayoutParams lp = new GridLayout.LayoutParams(GridLayout.spec(row, GridLayout.FILL), GridLayout.spec(colstart, colspan));
        return lp;
    }

    public static GridLayout.LayoutParams gridLayoutParamsRowSpan(int rowspan, int col) {
        GridLayout.LayoutParams lp = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.VERTICAL, rowspan), GridLayout.spec(col, GridLayout.FILL));
        return lp;
    }

    public static GridLayout.LayoutParams gridLayoutParams(int row, int column) {
        GridLayout.LayoutParams lp = new GridLayout.LayoutParams(GridLayout.spec(row, GridLayout.FILL, 1), GridLayout.spec(column, GridLayout.FILL, 1));
        return lp;
    }

    public static GridLayout.LayoutParams gridLayoutParams(int row, int column, float weightRow, float weightCol) {
        GridLayout.LayoutParams lp = new GridLayout.LayoutParams(GridLayout.spec(row, GridLayout.FILL, weightRow), GridLayout.spec(column, GridLayout.FILL, weightCol));
        return lp;
    }

    public static GridLayout.LayoutParams gridLayoutParams(int row, int column, int marginLeft, int marginTop, int marginRight, int marginBottom) {
        GridLayout.LayoutParams lp = Draw.gridLayoutParams(row, column);
        lp.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        return lp;
    }

    public static TextView getTextView(Context context, String text) {
        return getTextView(context, text, Gravity.LEFT, Gravity.TOP);
    }

    public static TextView getTextView(Context instance, String text, int horizontalPos, int verticalPos) {
        TextView textView = new TextView(instance);
        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
        textView.setGravity(horizontalPos | verticalPos);
        textView.setText(text);
        textView.setTextColor(Color.parseColor("#000000"));
        return textView;
    }

}
