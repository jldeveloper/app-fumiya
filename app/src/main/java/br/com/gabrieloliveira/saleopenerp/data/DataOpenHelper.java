package br.com.gabrieloliveira.saleopenerp.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Gabriel on 13/10/2015.
 */
public class DataOpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 60;
    private static final String DATABASE_NAME = "saleopenerp.db";
    private static final Map<String, Base> objects = new HashMap<String, Base>(){{put("sync", new Sync());put("res.users",  new User());put("res.partner", new Partner());put("sale.order", new Order());put("product.product", new Product());put("product.uom", new Uom());put("account.payment.term", new PaymentTerm());put("payment.type", new PaymentType());put("sale.order.line", new OrderLine());}};

    private HashMap<String, String> fields;

    private static DataOpenHelper instance;

    public static DataOpenHelper getInstance(Context context) {
        if (instance == null) instance = new DataOpenHelper(context);
        return instance;
    }
    private DataOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void createEntryTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS sys_table (id INTEGER PRIMARY KEY, name VARCHAR(128), fields TEXT, version INTEGER);";
        db.execSQL(sql);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        createEntryTable(db);
        Iterator iterator = this.objects.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            Base object = (Base) entry.getValue();
            if (!object.getCreateSql().isEmpty())
                db.execSQL(object.getCreateSql());
        }
    }

    public Cursor getTableData(SQLiteDatabase db, String table) {
        String sql = "SELECT * FROM sys_table WHERE name LIKE '" + table + "';";
        return db.rawQuery(sql, new String[]{});
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Agora não mais apagamos as tabelas, só editamos. Essa edição foi cancelada porque o sqlite não suporta a alteração de colunas
        Iterator iterator = this.objects.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            Base object = (Base) entry.getValue();
            // Verificamos se a tabela está registrada.
            /*Cursor c = getTableData(db, object.getTableName());
            if (c.moveToFirst()) {
                // Se a versão for a mesma não faz nenhuma alteração
                if (DATABASE_VERSION != c.getInt(c.getColumnIndex("version"))) {
                    resolveFields(c.getString(c.getColumnIndex("fields")));
                    db.execSQL(object.getUpdateSql(this.fields));
                    String str_fields = getCommaFields(object.getFieldsTypes());
                    String sql = "UPDATE sys_table SET version = " + DATABASE_VERSION + ", fields = '" + str_fields + "' WHERE id = " + c.getInt(c.getColumnIndex("id")) + ";";
                    db.execSQL(sql);
                }
            } else {
                // Se não estiver registrada, executamos o script de criação
                db.execSQL(object.getCreateSql());
                String str_fields = getCommaFields(object.getFieldsTypes());
                String sql = "INSERT INTO sys_table (name, fields, version) VALUES ('" + object.getTableName() + "', '" + str_fields + "', " + DATABASE_VERSION + ");";
                db.execSQL(sql);
            }*/
            String sql = "DROP TABLE IF EXISTS " + object.getTableNameSql() + ";";
            db.execSQL(sql);
        }
        onCreate(db);
    }

    public static Map<String, Base> getObjects() {
        return objects;
    }

    public void resolveFields(String fields) {
        this.fields = new HashMap<>();
        String[] fs = fields.split(",");
        for (String f: fs) {
            String value = f.trim();
            if (!value.contains(" ")) {
                this.fields.put(value, "");
            } else {
                String[] value_divided = value.split(" ");
                String key = value_divided[0];
                String key_value = "";
                if (value_divided.length > 1) {
                    for (int i = 1; i < value_divided.length; i++) {
                        if (i != 1) key_value += " ";
                        key_value += value_divided[i];
                    }
                }
                this.fields.put(key, key_value);
            }

        }
    }

    public String getCommaFields(Map<String, String> fields) {
        String fields_str = "";
        Iterator i = fields.entrySet().iterator();
        boolean is_first = true;
        while (i.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) i.next();
            String field = entry.getKey();
            String type = entry.getValue();
            if (!is_first) fields_str += ",";
            fields_str += field + " " + type;
            is_first = false;
        }
        return fields_str;
    }
}
