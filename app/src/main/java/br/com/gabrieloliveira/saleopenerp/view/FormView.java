package br.com.gabrieloliveira.saleopenerp.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.Gravity;
import android.view.View;
import android.support.v7.widget.GridLayout;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import br.com.gabrieloliveira.saleopenerp.MainActivity;
import br.com.gabrieloliveira.saleopenerp.R;
import br.com.gabrieloliveira.saleopenerp.data.Base;
import br.com.gabrieloliveira.saleopenerp.exception.UserException;
import br.com.gabrieloliveira.saleopenerp.lib.Draw;
import br.com.gabrieloliveira.saleopenerp.lib.MoneyTextWatcher;
import br.com.gabrieloliveira.saleopenerp.lib.Tools;

/**
 * Created by Gabriel on 26/01/2016.
 */
public class FormView extends BaseView {
    protected HashMap<String, View> views = new HashMap<>();
    protected HashMap<String, HashMap<String, Integer>> spinner_contents = new HashMap<>();
    /**
     * Armazena o id se for uma edição. Se não for edição não carrega nada no form
     */
    protected String id;
    /**
     * Contém o layout principal onde é carregado o formulário
     */
    protected GridLayout view;
    /**
     * Esse mapa contém os VALORES das relações. A chave de acesso é o nome da relação retornado no Mapa getFormRelations() do objeto data.Base
     * Dentro de cada relação podem existir n Mapas do tipo chave -> valor, que são respectivamente o nome do campo apontando para o seu valor.
     * Esses valores serão depois salvos no banco de dados
     */
    protected HashMap<String, ArrayList<HashMap<String, String>>> relations_values = new HashMap<>();
    /**
     * Esse atributo indica se ao clicar no botão salvar os dados devem ser salvos no banco ou fechar a janela de adição de item no caso de relações
     * Se for true, salva os dados no Banco de dados, false apenas fecha a janela e salva na relations_values do objeto FormView parent
     */
    protected boolean button_action_save = true;

    /**
     * Aponta para o Objeto FormView que é o pai desse, ou seja, o que abriu um Dialog contendo o FormView corrente.
     * Esse atributo é usado para salvar os valores das relações
     */
    protected FormView parent;
    /**
     * Armazena o nome da relação que é sempre o nome do campo relacionado
     */
    protected String relation_name;
    /**
     * Armazena o Dialog que é o container. No caso de FormView relacionado, para que seja possível fechá-lo ao clicar em salvar
     */
    protected Dialog dialog;

    /**
     * Armazena os containers da relação. A chave é o nome do campo relacionado vindo de getFormRelations() do @object
     * Esses objetos GridLayout são os que contém os registros das relações já adicionadas. São para exibir esses registros
     */
    protected HashMap<String, GridLayout> relations_container_list = new HashMap<>();

    public void addValueInRelation(String relation_name, HashMap<String, String> values) {
        if (relations_values.get(relation_name) == null) relations_values.put(relation_name, new ArrayList<HashMap<String, String>>());
        relations_values.get(relation_name).add(values);
    }

    public ArrayList<HashMap<String, String>> getValuesFromRelation(String relation_name) {
        return relations_values.get(relation_name);
    }
    public void setParent(FormView parent, String relation_name, Dialog dialog) {
        this.parent = parent;
        this.relation_name = relation_name;
        this.dialog = dialog;
    }
    public HashMap<String, View> getViews() {
        return views;
    }
    public void setButtonActionSave(boolean val) {
        button_action_save = val;
    }

    public boolean getButtonActionSave() {
        return button_action_save;
    }

    public void show(GridLayout view, String id) {
        this.view = view;
        this.id = id;
        object.find(id);
        show(view);
    }
    public void show(GridLayout view) {
        if (view == null) return;
        if (searchContainer != null) {
            searchContainer.removeAllViews();
        }
        this.view = view;
        if (object.get("id") != null && object.loaded) {
            // Garante que o id do objeto formulário contém o id
            id = object.get("id");
            // Carrega as relações
            Map<String, String> relations = object.getFormRelationObjects();
            Iterator iterator = relations.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = (Map.Entry) iterator.next();
                Base item = Base.getObjectByAppName(entry.getValue());
                List<Base> itens = item.list(entry.getKey() + " = ?", new String[]{object.get("res_id")});
                System.out.println("#DEBUG: " + itens.toString());
                for (Base o: itens) {
                    addValueInRelation(entry.getKey(), (HashMap) o.getData());
                }
            }
        }
        view.removeAllViews();
        ArrayList<String> form_fields = object.getFormFields();
        // Define o row count. O formulário sempre usa 2 colunas
        // É igual ao número de campos do formulário mais 2 de cada relação e mais 1 do botão Salvar
        view.setRowCount(form_fields.size() + (2 * object.getRelationsSize()) + 1);
        view.setColumnCount(2);
        List<String> fields_onchange = object.getOnChangeFields();
        int row = 0;
        int col = 0;

        for (final String field: form_fields) {
            String type = object.getFieldType(field);
            Draw.addViewInGrid(view, Draw.getTextView(context, object.getLabel(field), Gravity.LEFT, Gravity.CENTER), row, col);
            col++;
            switch (type) {
                case "INTEGER":
                    if (object.hasRelation(field)) {
                        // Nesse caso cria uma lista
                        final Spinner spinner = new Spinner(context);
                        Base relationObject = object.getRelationObject(field);
                        if (relationObject != null) {
                            // Verifica se há constraints
                            ArrayList<String[]> relatedConstraints = object.getRelatedConstraints(field);
                            if (relatedConstraints != null) {
                                for (String[] constraint: relatedConstraints) {
                                    relationObject.addConstraint(constraint);
                                }
                            }
                            List<Base> list = relationObject.list();
                            String[] items = new String[list.size()];
                            int index = 0;
                            spinner_contents.put(field, new HashMap<String, Integer>());
                            for (Base item: list) {
                                items[index] = item.getRelationListNameValue();
                                spinner_contents.get(field).put(item.get("res_id"), index);
                                index++;
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, items);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner.setAdapter(adapter);
                            addOnChangeEvent(fields_onchange, field, spinner);
                            if (object.loaded) {
                                System.out.println("OBJECT LOADED");
                                System.out.println(field);
                                if (object.get(field) == null) {
                                    System.out.println("NULL");
                                } else {
                                    System.out.println(object.get(field));
                                    spinner.setSelection(spinner_contents.get(field).get(object.get(field)));
                                }

                            }
                        }
                        //Draw.addViewInGrid(view, spinner, row, col);
                        addElement(view, spinner, row, col);
                        views.put(field, spinner);
                    } else {
                        EditText editText = new EditText(context);
                        editText.setInputType(InputType.TYPE_NUMBER_VARIATION_NORMAL);
                        if (object.loaded) {
                            editText.setText(object.get(field));
                        }
                        //Draw.addViewInGrid(view, editText, row, col);
                        addOnChangeEvent(fields_onchange, field, editText);
                        addElement(view, editText, row, col);
                        views.put(field, editText);
                    }
                    row++;
                    col = 0;
                    break;
                case "VARCHAR":
                case "TEXT":
                case "CHAR":
                    if (object.hasFormFieldOptions(field)) {
                        Spinner spinner = new Spinner(context);
                        spinner_contents.put(field, new HashMap<String, Integer>());
                        HashMap<String, String> options = object.getFormFieldOptions(field);
                        String[] items = new String[options.size()];
                        Iterator iterator = options.entrySet().iterator();
                        int index = 0;
                        while (iterator.hasNext()) {
                            Map.Entry<String, String> entry = (Map.Entry) iterator.next();
                            items[index] = entry.getValue();
                            spinner_contents.get(field).put(entry.getKey(), index);
                            index++;
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, items);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                        Draw.addViewInGrid(view, spinner, row, col);
                        views.put(field, spinner);
                        if (object.loaded) {
                            setSpinnerValue(spinner, field, object.get(field));
                        }
                    } else {
                        EditText editText = new EditText(context);
                        editText.setHint(object.getLabel(field));
                        addOnChangeEvent(fields_onchange, field, editText);
                        if (object.isFormReadOnly(field)) editText.setKeyListener(null);
                        Draw.addViewInGrid(view, editText, row, col);
                        if (object.loaded) {
                            editText.setText(object.get(field));
                        }
                        views.put(field, editText);
                    }
                    row++;
                    col = 0;
                    break;
                case "DECIMAL":
                case "FLOAT":
                case "DOUBLE":
                    EditText editTextDecimal = new EditText(context);
                    editTextDecimal.setHint(object.getLabel(field));
                    editTextDecimal.setInputType(InputType.TYPE_CLASS_NUMBER);
                    editTextDecimal.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    editTextDecimal.setKeyListener(DigitsKeyListener.getInstance(false, true));
                    //Draw.addViewInGrid(view, editTextDecimal, row, col);
                    addOnChangeEvent(fields_onchange, field, editTextDecimal);
                    //addElement(view, Draw.getTextView(context, "R$"), row, col);
                    if (object.isCurrency(field))
                        editTextDecimal.addTextChangedListener(new MoneyTextWatcher(editTextDecimal));
                    addElement(view, editTextDecimal, row, col);
                    if (object.isFormReadOnly(field)) editTextDecimal.setKeyListener(null);
                    if (object.loaded) {
                        editTextDecimal.setText(object.get(field));
                    }
                    views.put(field, editTextDecimal);
                    row++;
                    col = 0;
                    break;
                case "BOOLEAN":
                    Spinner spinner = new Spinner(context);
                    String[] items = new String[]{"Sim", "Não"};
                    spinner_contents.put(field, new HashMap<String, Integer>());
                    spinner_contents.get(field).put("0", 1);
                    spinner_contents.get(field).put("1", 0);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, items);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                    //Draw.addViewInGrid(view, spinner, row, col);
                    addElement(view, spinner, row, col);
                    if (object.loaded && object.get(field) != null) {
                        int position = 0;
                        if (object.get(field).equals("0")) position = 1;
                        spinner.setSelection(position);
                    }
                    addOnChangeEvent(fields_onchange, field, spinner);
                    views.put(field, spinner);
                    row++;
                    col = 0;
                    break;
            }
        }
        AtomicInteger rowa = new AtomicInteger(row);
        AtomicInteger cola = new AtomicInteger(col);
        addViewRelated(rowa, cola);
        addSaveButton(rowa, cola);
        if (object.loaded) {
            refreshRelations();
        }
    }

    public void addOnChangeEvent(List<String> fields_onchange, final String field, View view) {
        if (fields_onchange.contains(field)) {
            if (view instanceof Spinner) {
                final Spinner spinner = (Spinner) view;
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String value = getValueFromSpinner(field, spinner);
                        HashMap<String, String> dict = object.processOnchange(field, value, getValuesFromViews());
                        fillViewsDict(dict);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            } else if (view instanceof EditText) {
                final EditText text = (EditText) view;
                text.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            EditText local = (EditText) v;
                            HashMap<String, String> dict = object.processOnchange(field, local.getText().toString(), getValuesFromViews());
                            fillViewsDict(dict);
                        }
                    }
                });
            }
        }
    }

    public void addViewRelated(AtomicInteger row, AtomicInteger column) {
        Map<String, String> objects = this.object.getFormRelationObjects();
        if (objects.isEmpty()) return;
        // Vamos adicionar botões para cada número de objetos
        Iterator iterator = objects.entrySet().iterator();
        final FormView self = this;
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry) iterator.next();
            final String key = entry.getKey();
            String value = entry.getValue();
            final Base item = Base.getObjectByAppName(value);
            GridLayout containerList = new GridLayout(context);
            relations_container_list.put(key, containerList);
            //Draw.addViewInGrid(this.view, containerList, Draw.gridLayoutParamsColSpan(row.getAndIncrement(), 2, column.get()));
            addElement(view, containerList, row.getAndIncrement(), 2, column.get());

            Button button = new Button(context);
            button.setText("ADICIONAR " + item.getDisplayName());
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog nagDialog = new Dialog(context, android.R.style.Theme_Light_NoTitleBar);
                    nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    nagDialog.setCancelable(true);
                    RelativeLayout relativeLayout = new RelativeLayout(context);
                    GridLayout gridRelation = new GridLayout(context);
                    relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                    gridRelation.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                    try {
                        FormView formView = new FormView();
                        formView.setContext(context);
                        formView.setObject(item);
                        formView.setButtonActionSave(false);
                        formView.show(gridRelation);
                        formView.setParent(self, key, nagDialog);
                        relativeLayout.addView(gridRelation);
                        nagDialog.setContentView(relativeLayout);
                        nagDialog.show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
            });
            addElement(view, button, row.getAndIncrement(), 2, column.get());
            //Draw.addViewInGrid(this.view, button, Draw.gridLayoutParamsColSpan(row.getAndIncrement(), column.get(), 2));
            button.getLayoutParams().width = GridLayout.LayoutParams.MATCH_PARENT;
        }
    }

    public void addSaveButton(AtomicInteger row, AtomicInteger col) {
        Button button = new Button(context);
        button.setText("Salvar");
        if (button_action_save) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Base item = Base.getObjectByAppName(object.getAppClass());
                    try {
                        Base.beginTransaction();
                        if (item != null) {
                            // Carrega os dados atuais se o item está sendo atualizado
                            if (id != null) item.find(id);
                            for (String field : object.getFormFields()) {
                                Object value = views.get(field);
                                String value_final = "";
                                if (value instanceof EditText) {
                                    EditText text = (EditText) value;
                                    value_final = text.getText().toString();
                                } else if (value instanceof Spinner) {
                                    Spinner spinner = (Spinner) value;
                                    value_final = getValueFromSpinner(field, spinner);
                                }
                                item.set(field, value_final);
                            }
                            if (!validate(item)) {
                                return;
                            }
                            Long local_id;
                            if (id == null) {
                                item.setLocal();
                                item.setDraft();
                                local_id = item.insert();
                                item.set("res_id", local_id.toString());
                                item.set("id", local_id.toString());
                                item.update();
                            } else {
                                local_id = new Long(id);
                                // Apenas altera o registro
                                item.update();
                                item.createSyncUpdateEntry();
                            }
                            // Agora salva as relações
                            Iterator iterator = relations_values.entrySet().iterator();
                            while (iterator.hasNext()) {
                                Map.Entry<String, ArrayList<HashMap<String, String>>> entry = (Map.Entry) iterator.next();
                                // Vamos usar o campo chave com o campo relacional que será determinado
                                String field_related = entry.getKey();
                                ArrayList<String> relation_ids = new ArrayList<>();
                                for (HashMap<String, String> relation_value_item : entry.getValue()) {
                                    Base relation_value_object = Base.getObjectByAppName(object.getFormRelationObjects().get(field_related)); // Instancia um a cada iteração para que seja um novo objeto e não interfira no insert
                                    Iterator iteratorObject = relation_value_item.entrySet().iterator();
                                    while (iteratorObject.hasNext()) {
                                        Map.Entry<String, String> entry_item = (Map.Entry) iteratorObject.next();
                                        String entry_field = entry_item.getKey();
                                        String entry_value = entry_item.getValue();
                                        relation_value_object.set(entry_field, entry_value); // Atribui no objeto relacionado os itens
                                    }
                                    if (relation_value_item.get("id") == null) {
                                        relation_value_object.set(field_related, item.get("res_id"));
                                        relation_value_object.setLocal();
                                        relation_value_object.setDraft();
                                        Long o_id = relation_value_object.insert();
                                        // Atribui também o res_id para o mesmo id
                                        relation_value_object.set("res_id", o_id.toString());
                                        relation_value_object.set("id", o_id.toString());
                                        relation_value_object.update();
                                        System.out.println("REGISTRO DE ID " + o_id.toString() + " CRIADO. ###############");
                                        relation_value_object.traceData();
                                    } else {
                                        // Como por enquanto não há atualização dos registros. Vamos comentar a parte a seguir. Se for necessário alterar, voltamos
                                        /*relation_value_object.set(field_related, object.get("res_id"));
                                        relation_value_object.update();
                                        relation_value_object.createSyncUpdateEntry();
                                        System.out.println("REGISTRO DE ID " + relation_value_object.get("id") + " ATUALIZADO. ##########################");*/
                                    }
                                    System.out.println("$$$$$$ ORDER_ID: " + relation_value_object.get("order_id") + " ID do registro: " + relation_value_object.get("id"));

                                    // Salva o id para excluir os que já não existirem mais
                                    relation_ids.add(relation_value_object.get("id"));
                                }
                                if (id != null) {
                                    System.out.println("DELETAR RELACOES ######################");
                                    deleteRelations(relation_ids, field_related);
                                }
                            }
                            item.updateAfterRelationsSave();
                            Integer position = object.getFormPositionRedirect();
                            HashMap<String, String> map = null;
                            if (position != null) {
                                map = new HashMap<String, String>();
                                map.put("initial", position.toString());
                            }
                            // Como Estamos em uma transação. Vamos agora depois de salvar, validar tudo. Assim se algo estiver errado, disparamos uma exceção e cancela tudo
                            if (!item.validate()) {
                                throw new UserException(item.getValidateMessage());
                            }
                            Base.successfulTransaction();
                            Tools.startActivity(context, MainActivity.class, map);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        alert("Erro", e.getMessage());
                    } finally {
                        Base.endTransaction();
                    }
                }
            });
        } else {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Nesse caso ele apenas salva em algum lugar para depois salvar no banco
                    HashMap<String, String> values = getValuesFromViews();
                    Iterator iterator = values.entrySet().iterator();
                    HashMap<String, String> fields_values = new HashMap<>();
                    while (iterator.hasNext()) {
                        Map.Entry<String, String> entry = (Map.Entry) iterator.next();
                        String field = entry.getKey();
                        String value = entry.getValue();
                        fields_values.put(field, value);
                    }
                    if (!validate(Base.getObjectByAppName(object.getAppClass()), fields_values))
                        return;
                    parent.addValueInRelation(relation_name, fields_values);
                    dialog.hide();
                    parent.refreshRelations();
                }
            });
        }

        //Draw.addViewInGrid(view, button, Draw.gridLayoutParamsColSpan(row.getAndIncrement(), col.get(), 2));
        addElement(view, button, row.getAndIncrement(), 2, col.get());

    }

    private void deleteRelations(ArrayList<String> relation_ids, String field_name) {
        Base object = Base.getObjectByAppName(this.object.getFormRelationObjects().get(field_name));
        List<String> ids = object.idsList(field_name + " = ?", new String[]{this.object.get("res_id")});
        for (String id: ids) {
            if (!relation_ids.contains(id)) {
                // Deleta o objeto se ele não estiver na lista
                object.find(id);
                System.out.println("VAI DELETAR O REGISTRO de NOME: " + object.get("name") + " e ID: " + object.get("id") + " COM RES_ID:" + object.get("res_id"));
                object.createSyncDeleteEntry(object.get("res_id"));
                object.delete();
            }
        }
    }

    public String getValueFromSpinner(String field, Spinner spinner) {
        HashMap<String, Integer> spinner_values = spinner_contents.get(field);
        if (spinner_values == null) return "";
        Iterator iterator = spinner_values.entrySet().iterator();
        Integer spinnerValue = spinner.getSelectedItemPosition();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = (Map.Entry) iterator.next();
            String id = entry.getKey();
            Integer value = entry.getValue();
            if (value == spinnerValue) {
                return id;
            }
        }
        return "";
    }

    public HashMap<String, String> getValuesFromViews() {
        HashMap<String, String> values = new HashMap<>();
        for (String field : object.getFormFields()) {
            Object value = views.get(field);
            String value_final = "";
            if (value instanceof EditText) {
                EditText text = (EditText) value;
                value_final = text.getText().toString();
            } else if (value instanceof Spinner) {
                Spinner spinner = (Spinner) value;
                value_final = getValueFromSpinner(field, spinner);
            }
            values.put(field, value_final);
        }
        return values;
    }

    public void refreshRelations() {
        Iterator iterator = relations_container_list.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, GridLayout> entry = (Map.Entry) iterator.next();
            final String key = entry.getKey();
            if (relations_values.get(key) == null) continue;
            GridLayout view = entry.getValue();
            // Limpa a view e adiciona novamente
            view.removeAllViews();
            Base object = Base.getObjectByAppName(this.object.getFormRelationObjects().get(key));
            ArrayList<String> fields = object.getFormFieldsList();
            int row = 0;
            int col = 0;
            for (String field: fields) {
                // Adiciona o header
                addElement(view, object.getLabel(field), row, col);
                col++;
            }
            // Adiciona o header de exclusão
            addElement(view, "EXCLUIR", row, col);
            row++;
            col = 0;
            for (final HashMap<String, String> value: relations_values.get(key)) {
                for (String field: fields) {
                    object.set(field, value.get(field)); // Informa todos no objeto para que ele calcule o valor de getListValue()
                }
                for (String field: fields) {
                    addElement(view, object.getListValue(field), row, col);
                    col++;
                }
                TextView txt = (TextView) addElement(view, "E", row, col);
                txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        relations_values.get(key).remove(value);
                        refreshRelations();
                    }
                });
                row++;
                col = 0;
            }
            // Agora adiciona as colunas dos totais
            List<String> values = this.object.getColumnsRelationTotals(relations_values.get(key));
            if (values != null) {
                for (String value: values) {
                    addElement(view, value, row, col);
                    col++;
                }
                addElement(view, "", row, col); // relacionado ao excluir
                row++;
                col = 0;
            }
        }
    }

    public View addElement(GridLayout view, String text, int line, int column) {
        String[] colors = new String[]{"#CCCCCC", "#FFFFFF"};
        String color = colors[line % 2];
        TextView txt = Draw.getTextView(this.context, text);
        txt.setHorizontallyScrolling(false);
        GridLayout.LayoutParams params = Draw.gridLayoutParams(line, column);
        params.width = 0;
        txt.setBackgroundColor(Color.parseColor(color));
        txt.getLayoutParams().width = 0;
        txt.setPadding(10, 10, 10, 10);
        txt.setClickable(true);
        Draw.addViewInGrid(view, txt, params);
        return txt;
        //view.requestFocus();
    }

    public void addElement(GridLayout view, View object, int line, int column) {
        GridLayout.LayoutParams params = Draw.gridLayoutParams(line, column);
        addElement(view, object, params);
    }

    public void addElement(GridLayout view, View object, GridLayout.LayoutParams params) {
        params.width = 0;
        object.setPadding(10, 10, 10, 10);
        object.setClickable(true);
        Draw.addViewInGrid(view, object, params);
        if (object.getLayoutParams() != null) {
            object.getLayoutParams().width = GridLayout.LayoutParams.MATCH_PARENT;
        }
    }

    public void addElement(GridLayout view, View object, int line, int colspan, int start) {
        GridLayout.LayoutParams params = Draw.gridLayoutParamsColSpan(line, start, colspan);
        addElement(view, object, params);
    }

    public void fillViewsDict(HashMap<String, String> dict) {
        Iterator iterator = dict.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry) iterator.next();
            String field = entry.getKey();
            String value = entry.getValue();
            View v = views.get(field);
            if (v instanceof EditText) {
                EditText e = (EditText) v;
                e.setText(value);
            } else if (v instanceof Spinner) {
                setSpinnerValue((Spinner) v, field, value);
            }
        }
    }

    public void setSpinnerValue(Spinner spinner, String field, String value) {
        HashMap<String, Integer> dict = spinner_contents.get(field);
        if (dict == null) return;
        if (spinner == null) return;
        if (value == null) return;
        spinner.setSelection(dict.get(value));
    }

    /**
     * Valida relações
     * @param item
     * @param data
     * @return
     */
    public boolean validate(Base item, Map<String, String> data) {
        // Vamos validar as contraints unique primeiro.
        ArrayList<HashMap<String, String>> values = parent.getValuesFromRelation(relation_name);
        if (values != null && values.size() > 0) {
            List<String[]> unique_constraints = object.getFormFieldsUnique();
            // Para cada constraint eu verifico se existe no objeto sendo inserido
            for (String[] unique: unique_constraints) {
                for (HashMap<String, String> value: values) {
                    if (unique.length == 1) {
                        if (data.get(unique[0]).equals(value.get(unique[0]))) {
                            // Se existe o valor igual para uma unique constraint já retorna false para o método
                            item.setFormFieldUniqueError(unique[0]);
                            processValidateError(item, "unique");
                            return false;
                        }
                    } else {
                        boolean all_equal = true;
                        for (String unique_field: unique) {
                            if (!data.get(unique).equals(value.get(unique))) all_equal = false;
                        }
                        if (all_equal) {
                            item.setFormFieldUniqueError(unique[0]);
                            processValidateError(item, "unique");
                            return false;
                        }
                    }
                }
            }
        }
        Iterator iterator = data.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry) iterator.next();
            item.set(entry.getKey(), entry.getValue());
        }
        return validate(item);
    }
    public boolean validate(Base item) {
        if (!item.validate()) {
            // Aponta para o campo com erro
            processValidateError(item, item.getValidateErrorType());
            return false;
        }
        return true;
    }

    public void processValidateError(Base item, String type) {
        String message = "";
        View value = null;
        switch (type) {
            case "required":
                message = "O campo " + item.getLabel(item.getFormFieldRequiredError()) + " é obrigatório";
                value = views.get(item.getFormFieldRequiredError());
                break;
            case "unique":
                message = "Já existe um registro com o mesmo valor para o campo " + item.getLabel(item.getFormFieldUniqueError());
                value = views.get(item.getFormFieldUniqueError());
                break;
            case "custom":
                message = item.getValidateMessage();
                value = views.get(item.getValidateFieldError());
                break;
        }
        if (value == null) return;
        if (value instanceof EditText) {
            EditText value_text = (EditText) value;
            value_text.setError(message);
            value_text.requestFocus();
        } else if (value instanceof Spinner) {
            final Spinner spinner = (Spinner) value;
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle("Erro");
            alert.setCancelable(true);
            alert.setMessage(message);
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    spinner.requestFocus();
                }
            });
            alert.show();

        }
    }

    protected void alert(String title, String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setCancelable(true);
        alert.setMessage(message);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alert.show();
    }
}

