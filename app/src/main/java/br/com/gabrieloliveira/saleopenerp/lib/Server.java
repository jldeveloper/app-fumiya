package br.com.gabrieloliveira.saleopenerp.lib;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.gabrieloliveira.saleopenerp.SaleOpenerp;
import br.com.gabrieloliveira.saleopenerp.data.User;
import br.com.gabrieloliveira.saleopenerp.exception.UserException;
import de.timroes.axmlrpc.XMLRPCClient;
import de.timroes.axmlrpc.XMLRPCException;

/**
 * Created by Gabriel on 20/01/2016.
 */
public class Server {
    protected XMLRPCClient client;
    protected String database = "openerpestribos";
    protected Integer uid;
    protected String password;
    public boolean isTest = false;

    public Server() {
        tryAttrib();
    }

    public void tryAttrib() {
        User user = User.getUser();
        if (user != null) {
            password = user.get("password");
            uid = Integer.parseInt(user.get("res_id"));
        }
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }
    public Integer getUid() {
        return this.uid;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getPassword() {
        return this.password;
    }

    protected static Server instance;
    public static Server instance() {
        if (instance == null)
            instance = new Server();
        if (instance.getPassword() == null || instance.getUid() == null) instance.tryAttrib();
        return instance;
    }

    public void connect(String type) {
        String action;
        if (type == "login") {
            action = "/xmlrpc/common";
        } else {
            action = "/xmlrpc/object";
        }
        try {
            String server = "162.243.83.106";
            int port = 8069;
            if (isTest) {
                server = "192.168.0.100";
                port = 8080;
                database = "erpestribos";
            }
            client = new XMLRPCClient(new URL("http", server, port, action));
        } catch (MalformedURLException e) {
            // pass like Python :)
        }
    }

    public void connectLogin() {
        connect("login");
    }

    public void connectObject() {
        connect("object");
    }


    public boolean login(String username, String password) throws UserException {
        connectLogin();
        // Esse método de login necessita que o usuário possua conexão. Por isso testa e retorna uma exceção caso nao esteja
        checkConnected();
        try {
            Object response = client.call("login", database, username, password);
            if (Tools.isBoolean(response)) {
                throw new UserException("Usuário ou senha inválidos");
            } else {
                uid = (Integer) response;
            }
        } catch (XMLRPCException e) {
            throw new UserException("Ocorreu um erro ao acessar o servidor. Verifique a conexão com a internet");
        }
        return true;
    }

    /**
     * Esse método de leitura retorna apenas um registro, informado pelo Integer id
     * @param object
     * @param id
     * @param fields
     * @return
     * @throws UserException
     */
    public Map<String, Object> read(String object, Integer id, List<String> fields) throws UserException {
        List<Integer> ids = new ArrayList<>();
        ids.add(id);
        List<Map<String, Object>> result = read(object, ids, fields);
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    /**
     * Esse método é utilizado para requisitar uma lista de objetos
     * @param object
     * @param ids
     * @param fields
     * @return
     * @throws UserException
     */
    public List<Map<String, Object>> read(String object, List<Integer> ids, List<String> fields) throws UserException {
        checkConnected();
        connectObject();
        try {
            // Adiciona no contexto a linguagem pt_BR para trazer os nomes em português
            HashMap<String, String> context = new HashMap<>();
            context.put("lang", "pt_BR");
            Object response = client.call("execute", database, uid, password, object, "read", ids, fields, context);
            if (Tools.isBoolean(response)) {
                throw new UserException("Erro ao ler no servidor. Por favor, efetue logout e entre novamente no app");
            }
            Object[] responseData = (Object[]) response;
            List<Map<String, Object>> result = new ArrayList<>();
            for (int i = 0; i < responseData.length; i++) {
                result.add((Map<String, Object>) responseData[i]);
            }
            return result;
        } catch (XMLRPCException e) {
            throw new UserException("Erro ao ler no servidor. Verifique se o OpenERP está online");
        }
    }

    public ArrayList<Integer> search(String object) throws UserException {
        return search(object, new ArrayList<String[]>());
    }

    public ArrayList<Integer> search(String object, ArrayList<String[]> params) throws UserException {
        checkConnected();
        connectObject();
        try {
            if (password == null || uid == null) {
                System.out.println(password);
                System.out.println(uid);
                throw new UserException("Os campos senha ou uid do objeto sync não são válidos.");
            }
            Object response = client.call("execute", database, uid, password, object, "search", params);
            checkBoolean(response);
            Object[] responseData = (Object[]) response;
            ArrayList<Integer> result = new ArrayList<>();
            for (int i = 0; i < responseData.length; i++) {
                result.add((Integer) responseData[i]);
            }
            return result;
        } catch (XMLRPCException e) {
            e.printStackTrace();
            throw new UserException("Erro ao ler no servidor. Verifique se o OpenERP está online");
        }
    }

    public Integer create(String object, HashMap<String, Object> data) throws UserException {
        checkConnected();
        connectObject();
        try {
            HashMap<String, String> context = new HashMap<>();
            context.put("lang", "pt_BR");
            Object response = client.call("execute", database, uid, password, object, "create", data, context);
            checkBoolean(response);
            return (Integer) response;
        } catch (XMLRPCException e) {
            e.printStackTrace();

            throw new UserException("Erro ao criar registro no servidor. OBJECT: " + object + ". DICT: " + getDebugDict(data) + ". MESSAGE: " + e.getMessage());
        }
    }

    public String getDebugDict(HashMap<String, Object> data) {
        String result = "";
        Iterator iterator = data.entrySet().iterator();
        int inicial = 0;
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = (Map.Entry) iterator.next();
            String key = entry.getKey();
            String value = Tools.getString(entry.getValue());
            if (inicial == 0) result += "{";
            else result += ", ";
            result += key + ": " + value;
            inicial++;
            if (inicial == data.size()) {
                result += "}";
            }
        }
        return result;
    }

    public boolean write(String object, ArrayList<Integer> ids, HashMap<String, Object> data) throws UserException {
        checkConnected();
        connectObject();
        try {
            HashMap<String, String> context = new HashMap<>();
            context.put("lang", "pt_BR");
            Object response = client.call("execute", database, uid, password, object, "write", ids, data, context);
            if ((Boolean)response) {
                return true;
            }
            return false;
        } catch (XMLRPCException e) {
            throw new UserException(e.getMessage());
        }
    }

    public boolean delete(String object, ArrayList<Integer> ids) throws UserException {
        checkConnected();
        connectObject();
        try {
            Object response = client.call("execute", database, uid, password, object, "unlink", ids);
            if ((Boolean) response) {
                return true;
            }
            return false;
        } catch (XMLRPCException e) {
            throw new UserException(e.getMessage());
        }
    }

    public void checkBoolean(Object o) throws UserException {
        checkBoolean(o, "Erro ao ler no servidor. Por favor, efetue logout e entre novamente no app");
    }

    public void checkBoolean(Object o, String message) throws UserException {
        if (Tools.isBoolean(o)) {
            throw new UserException(message);
        }
    }



    public static boolean isConnected() throws UserException {
        Context context = SaleOpenerp.getContext();
        try {
            ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo i = conMgr.getActiveNetworkInfo();
            if (i == null)
                return false;
            if (!i.isConnected())
                return false;
            if (!i.isAvailable())
                return false;
            return true;
        } catch (NullPointerException e) {
            throw new UserException("Erro em isConnected(): " + e.getMessage());
        }
    }

    public static boolean checkConnected() throws UserException {
        if (isConnected()) return true;
        throw new UserException("Você não está conectado à internet");
    }
}
