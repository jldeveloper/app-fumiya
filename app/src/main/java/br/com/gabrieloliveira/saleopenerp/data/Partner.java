package br.com.gabrieloliveira.saleopenerp.data;

import android.content.Context;
import android.support.v7.widget.GridLayout;
import android.view.View;

import java.util.ArrayList;

import br.com.gabrieloliveira.saleopenerp.exception.UserException;
import br.com.gabrieloliveira.saleopenerp.view.BaseView;

/**
 * Created by Gabriel on 20/01/2016.
 */
public class Partner extends Base {
    public Partner() {
        TABLE_NAME = "partner";
        FIELDS_SEARCH = new String[]{"name", "legal_name", "cnpj_cpf", "inscr_est"};
        init();
        addFieldDefinition("name", "VARCHAR(128)");
        addFieldDefinition("legal_name", "VARCHAR(128)");
        addFieldDefinition("inscr_est", "VARCHAR(16)");
        addFieldDefinition("cnpj_cpf", "VARCHAR(18)");
        addFieldDefinition("phone", "VARCHAR(64)");
        addFieldDefinition("email", "VARCHAR(240)");
        addFieldDefinition("vendasnomes", "BOOLEAN");
        addFieldDefinition("qty_days_last_order", "INTEGER");
        addFieldDefinition("limit_c", "DECIMAL(10,2)");
        addFieldDefinition("blocked", "BOOLEAN");
        addFieldDefinition("bloqueado_text", "VARCHAR(1)");
        addFieldDefinition("inadimplente", "BOOLEAN");
        EXTERNAL_FIELDS = new String[]{"name", "legal_name", "inscr_est", "cnpj_cpf", "phone", "email", "vendasnomes", "qty_days_last_order", "limit_c", "blocked", "bloqueado_text", "inadimplente"};
        APP_OBJECT = "res.partner";
        DISPLAY_NAME = "Clientes";
        INCLUDE_SEARCH = false;
        addFieldList("legal_name", "name", "qty_days_last_order");
        addLabel("name", "Nome Fantasia");
        addLabel("legal_name", "Razão Social");
        addLabel("vendasnomes", "Última Compra");
        addLabel("bloqueado_text", "Bloqueado");
        addLabel("qty_days_last_order", "Última Compra");
        addListConstraint("customer", "=", "True");
    }

    public String getListValue(String field) {
        String value = super.getListValue(field);
        if (field.equals("qty_days_last_order")) {
            if (value.equals("0")) value = "NÃO COMPROU";
            else value = value + " DIA(S)";
        }
        if (field.equals("name")) {
            String bloqueado = get("bloqueado_text");
            if (bloqueado != null)
                return value + " (" + bloqueado + ")";
        }
        return value;
    }

    public String getDefaultOrder() {
        return "name ASC";
    }

    public void addClickListener(final Context context, final GridLayout layout, final String field, final Base item, View v, final GridLayout gridSearch) {
        switch (field) {
            case "name":
            case "legal_name":
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Order order = new Order();
                        order.setLoaded();
                        order.set("partner_id", item.get("res_id"));
                        order.set("gen_nfe", "30");
                        try {
                            BaseView.route(context, "Form", order, layout, gridSearch);
                        } catch (UserException e) {
                            e.printStackTrace();
                        }
                    }
                });
                break;
        }
    }
}
