package br.com.gabrieloliveira.saleopenerp.data;

/**
 * Created by Gabriel on 03/02/2016.
 */
public class PaymentTerm extends Base {
    public PaymentTerm() {
        initObject();
    }
    public void initObject() {
        init();
        TABLE_NAME = "payment_term";
        APP_OBJECT = "account.payment.term";
        FIELDS_SEARCH = new String[]{"name"};
        addFieldDefinition("name", "VARCHAR(64)");
        addFieldDefinition("indPag", "VARCHAR(32)");
        addFieldDefinition("active", "BOOLEAN");
        addFieldDefinition("cheque", "BOOLEAN");
        addFieldDefinition("boleto", "BOOLEAN");
        addFieldDefinition("payment_type", "INTEGER");
        EXTERNAL_FIELDS = new String[]{"name", "indPag", "active", "cheque", "boleto", "payment_type"};
        addLabel("name", "Nome");
        addLabel("indPag", "Indicador de Pagamento");
        addLabel("active", "Ativo");
        addLabel("cheque", "Cheque");
        addLabel("boleto", "Boleto");
        addLabel("payment_type", "Tipo de Pagamento");
        addRelation("payment_type", "payment.type");
    }
}
