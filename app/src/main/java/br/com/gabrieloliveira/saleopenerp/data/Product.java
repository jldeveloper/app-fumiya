package br.com.gabrieloliveira.saleopenerp.data;

import br.com.gabrieloliveira.saleopenerp.lib.Tools;

/**
 * Created by Gabriel on 21/01/2016.
 */
public class Product extends Base {
    protected String mode;

    public void setMode(String mode) {
        this.mode = mode;
        /*switch (this.mode) {
            case "can_sale":
                addConstraint("sale_ok", "=", "1");
                addConstraint("active", "=", "1");
                break;
        }*/
    }

    public void initProduct() {
        init();
        TABLE_NAME = "product";
        addFieldDefinition("name", "VARCHAR(128)");
        addFieldDefinition("default_code", "VARCHAR(64)");
        addFieldDefinition("uom_id", "INTEGER");
        addFieldDefinition("virtual_available", "DECIMAL(12,3)");
        addFieldDefinition("qty_available", "DECIMAL(12,3)");
        addFieldDefinition("standard_price", "DECIMAL(8,2)");
        addFieldDefinition("prontaentrega", "DECIMAL(8,2)");
        addFieldDefinition("sale_ok", "BOOLEAN");
        addFieldDefinition("active", "BOOLEAN");
        addFieldDefinition("list_price", "DECIMAL(8,2)");
        addFieldDefinition("minimal_price", "DECIMAL(8,2)");
        addFieldDefinition("disponibilidade", "VARCHAR(64)");
        DISPLAY_NAME = "Produtos";
        EXTERNAL_FIELDS = new String[]{"name", "default_code", "uom_id", "virtual_available", "standard_price", "prontaentrega", "sale_ok", "active", "qty_available", "list_price", "minimal_price", "disponibilidade"};
        APP_OBJECT = "product.product";
        FIELDS_SEARCH = new String[]{"name", "default_code"};
        addFieldList("name", "qty_available", "list_price");
        addLabel("name", "Nome");
        addLabel("default_code", "Referência");
        addLabel("uom_id", "Unidade de Medida");
        addLabel("virtual_available", "Estoque disponível");
        addLabel("standard_price", "Preço de custo");
        addLabel("prontaentrega", "Preço Atacado");
        addLabel("minimal_price", "Preço Mínimo");
        addLabel("list_price", "Preço Base");
        addConstraint("sale_ok", "=", "1");
        addConstraint("active", "=", "1");
        addConstraint("disponibilidade", "NOT LIKE", "personalizado_total");
    }

    public Product(String mode) {
        initProduct();
        setMode(mode);
    }
    public Product() {
        initProduct();
    }

    public String getListValue(String field) {
        String value = super.getListValue(field);
        if (field.equals("list_price")) {
            value = Tools.getCurrencyFormat(get(field));
            Uom uom = new Uom(get("uom_id"));
            value = value + " (" + uom.get("name") + ")";
        }
        return value;
    }
}
