package br.com.gabrieloliveira.saleopenerp.view;

import android.content.Context;
import android.support.v7.widget.GridLayout;
import android.widget.RelativeLayout;

import java.lang.reflect.Array;
import java.util.ArrayList;

import br.com.gabrieloliveira.saleopenerp.data.Base;
import br.com.gabrieloliveira.saleopenerp.exception.UserException;

/**
 * Created by Gabriel on 26/01/2016.
 */
public abstract class BaseView {
    protected Base object;
    protected Context context;
    protected GridLayout searchContainer;
    protected ArrayList<String[]> constraints;

    public void setObject(Base object) {
        this.object = object;
    }
    public void setSearchContainer(GridLayout layout) {
        searchContainer = layout;
    }
    public Base getObject() {
        return object;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static BaseView route(Context context, String viewType, Base object, GridLayout widgetView, GridLayout layout, ArrayList<String[]> constraints) throws UserException {
        // Primeiro tenta pegar um objeto do mesmo tipo do objeto que está sendo passado
        String objectClassName = object.getClass().getSimpleName();
        BaseView view = getObject(objectClassName + viewType + "View");
        if (view == null)
            view = getObject(viewType + "View"); // Se não conseguir instanciar uma view específica, então instancia a view genérica que pode ser ListView, FormView ou StaticView
        if (view == null) {
            throw new UserException("Operação Inválida");
        }
        if (object != null)
            view.setObject(object);
        view.setContext(context);
        if (layout != null)
            view.setSearchContainer(layout);
        if (constraints != null)
            view.setConstraints(constraints);
        // O método show deve realizar processo através de uma asyncview
        view.show(widgetView);
        return view;
    }

    public static BaseView route(Context context, String viewType, Base object, GridLayout widgetView, GridLayout layout) throws UserException {
        return route(context, viewType, object, widgetView, layout, null);
    }


    public static BaseView getObject(String className) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName("br.com.gabrieloliveira.saleopenerp.view." + className);
            return (BaseView) clazz.newInstance();
        } catch (InstantiationException e) {
            System.out.println(e.getMessage());
        } catch (IllegalAccessException e) {
            System.out.println(e.getMessage());
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public abstract void show(GridLayout view);

    public void setConstraints(ArrayList<String[]> constraints) {
        this.constraints = constraints;
    }
}
