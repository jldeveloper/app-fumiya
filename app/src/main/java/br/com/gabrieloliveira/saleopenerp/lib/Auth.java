package br.com.gabrieloliveira.saleopenerp.lib;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.gabrieloliveira.saleopenerp.data.Base;
import br.com.gabrieloliveira.saleopenerp.data.DataOpenHelper;
import br.com.gabrieloliveira.saleopenerp.data.User;
import br.com.gabrieloliveira.saleopenerp.exception.UserException;

/**
 * Created by Gabriel on 20/01/2016.
 * Responsavel por gerenciar o login e logout do usuário no sistema.
 * O usuário terá que se logar ao menos uma vez estando online. Após isso, o sistema local do android salvará suas credenciais
 * localmente para usá-las no processo de sincronização, que sempre ocorrerá online.
 * Para estar logado ele precisará estar com o campo key válido na base de dados
 */
public class Auth {
    protected String username;
    protected String password;

    protected User user;

    protected static Auth instance;
    public static Auth instance() {
        if (instance == null) instance = new Auth();
        return instance;
    }
    public Auth() {
        user = new User();
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getPassword() {
        return this.password;
    }

    public boolean login(String username, String password) throws UserException {
        setUsername(username);
        setPassword(password);
        if (Server.instance().login(username, password)) {
            List<Base> objects = this.user.list();
            if (objects.isEmpty()) {
                // Requisita os dados sobre o usuário no servidor
                // Cria
                this.user.set("username", username);
                this.user.set("password", password);
                this.user.set("res_id", Server.instance().getUid().toString());
                this.user.set("logged", "1");
                this.user.insert();
            }
            return true;
        }
        return false;
    }

    public boolean isLogged() {
        List<Base> objects = this.user.list();
        if (objects.isEmpty()) return false;
        User user = (User)objects.get(0);
        if (user.get("logged").equals("1")) return true;
        return false;
    }

    /**
     * Verifica se não existe nenhum registro local. Se existir retorna false. Esse método é apenas utilitário, não é chamdo antes de efetuar logout para o usuário
     * @return
     */
    public boolean canLogout() {
        br.com.gabrieloliveira.saleopenerp.data.Sync sync = new br.com.gabrieloliveira.saleopenerp.data.Sync();
        List<Base> list = sync.list("sync = ?", new String[]{"0"});
        return list.isEmpty();
    }

    public boolean  logout() {
        // Elimina todos os usuários da tabela
        this.user.deleteAll();
        // Elimina também todos os dados
        Map<String, Base> objects = DataOpenHelper.getObjects();
        Iterator iterator = objects.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Base> entry = (Map.Entry) iterator.next();
            entry.getValue().deleteAll();
        }
        return true;
    }
}
