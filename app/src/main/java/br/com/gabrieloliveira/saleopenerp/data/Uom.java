package br.com.gabrieloliveira.saleopenerp.data;

/**
 * Created by Gabriel on 01/02/2016.
 */
public class Uom extends Base {
    public Uom() {
        initUom();
    }

    public Uom(String res_id) {
        initUom();
        findByRes(res_id);
    }

    public void initUom() {
        init();
        TABLE_NAME = "uom";
        addFieldDefinition("name", "VARCHAR(128)");
        addFieldDefinition("category_id", "INTEGER");
        addFieldDefinition("active", "BOOLEAN");
        addFieldDefinition("uom_type", "VARCHAR(128)");
        addFieldDefinition("rounding", "DECIMAL(8,3)");

        DISPLAY_NAME = "Unidade de Medida";
        EXTERNAL_FIELDS = new String[]{"name", "category_id", "active", "uom_type", "rounding"};
        APP_OBJECT = "product.uom";
        FIELDS_SEARCH = new String[]{"name"};
    }
}
