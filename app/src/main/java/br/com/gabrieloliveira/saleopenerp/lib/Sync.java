package br.com.gabrieloliveira.saleopenerp.lib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.gabrieloliveira.saleopenerp.data.Base;
import br.com.gabrieloliveira.saleopenerp.data.Order;
import br.com.gabrieloliveira.saleopenerp.data.OrderLine;
import br.com.gabrieloliveira.saleopenerp.data.Partner;
import br.com.gabrieloliveira.saleopenerp.data.PaymentTerm;
import br.com.gabrieloliveira.saleopenerp.data.PaymentType;
import br.com.gabrieloliveira.saleopenerp.data.Product;
import br.com.gabrieloliveira.saleopenerp.data.Uom;
import br.com.gabrieloliveira.saleopenerp.data.User;
import br.com.gabrieloliveira.saleopenerp.exception.UserException;

/**
 * Created by Gabriel on 20/01/2016.
 */
public class Sync {

    public static boolean SYNCING = false;
    public Sync() {
        server = Server.instance();
    }
    protected static Sync instance;
    public static Sync instance() {
        if (instance == null)
            instance = new Sync();
        return instance;
    }

    protected Server server;

    protected Base[] reads = new Base[]{new Order(), new Partner(), new Product(), new Uom(), new PaymentTerm(), new PaymentType(), new OrderLine()};

    /**
     * Este método pega os registros que estão na tabela sync e os envia para o servidor atualizando os campos.
     * Após isso ele executa uma sincronia dos dados locais. Lendo os registros de cada tipo de dado informado no atributo reads
     */
    public void sync() throws UserException {
        if (SYNCING) return;
        SYNCING = true;
        try {
            syncWrite();
            syncRead();
        } catch (UserException e) {
            // Atribui que não está mais sincronizando e continua a exceção para os níveis mais altos
            SYNCING = false;
            // Atribui erro aos registros que ainda não foram sincronizados
            br.com.gabrieloliveira.saleopenerp.data.Sync sync = new br.com.gabrieloliveira.saleopenerp.data.Sync();
            ArrayList<Base> items = (ArrayList<Base>) sync.list("sync = ?", new String[]{"0"}, "sync_priority ASC");
            for (Base item: items) {
                item.set("sync_failed", "1");
                item.set("sync_error", e.getMessage());
                item.update();
            }
            throw e;
        } finally {
            SYNCING = false;
        }
        SYNCING = false;
    }

    /**
     * Lê os registros da tabela sync e faz a atualização no servidor de cada um
     */
    public void syncWrite() throws UserException {
        br.com.gabrieloliveira.saleopenerp.data.Sync sync = new br.com.gabrieloliveira.saleopenerp.data.Sync();
        // Primeiro executa os registros com prioridade de número mais baixo, iniciando com 0. Todos por padrão têm 0, conforme dependências eles devem ser mudados
        ArrayList<Base> items = (ArrayList<Base>) sync.list("sync = ?", new String[]{"0"}, "sync_priority ASC");
        for (Base item: items) {
            Base object = item.getObjectByAppName(item.get("resource"));
            if (!object.findByRes(item.get("res_id")) && !item.get("is_delete").equals("1")) {
                System.out.println("Is_Create: " + item.get("is_create") + " - Object " + item.get("resource") + " de ID: " + item.get("res_id") + " NOT FOUND");
                continue;
            }
            if (item.get("is_create").equals("1")) {
                // Faz a chamada create
                if (object.hasDeppends()) {
                    // Altera os valores. Já que o registro pode estar apontando para um id local
                    Iterator iterator = object.getDeppends().entrySet().iterator();
                    while (iterator.hasNext()) {
                        Map.Entry<String, String> entry = (Map.Entry) iterator.next();
                        String field = entry.getKey();
                        String className = entry.getValue();
                        // Aí abrimos o objeto da classe e procuramos pelo id que está apontando localmente e vamos mudar os dados enviados
                        Base objectRelated = Base.getObjectByAppName(className);
                        objectRelated.find(object.get(field));
                        if (objectRelated.isLocal() || !objectRelated.get("id").equals(objectRelated.get("res_id"))) {
                            object.set(field, objectRelated.get("res_id")); // Aqui altera efetivamente a relação para apontar para o id externo
                        }
                    }
                }
                Integer id = server.create(object.getAppClass(), object.getExternalData());
                object.saveSync(id);
                item.saveSync(id);
            } else {
                if (item.get("is_delete").equals("1")) {
                    ArrayList<Integer> ids = new ArrayList<>();
                    object.traceData();
                    ids.add(Integer.parseInt(item.get("res_id")));
                    if (server.delete(object.getAppClass(), ids)) {
                        item.saveSync();
                    }
                } else {
                    ArrayList<Integer> ids = new ArrayList<>();
                    ids.add(Integer.parseInt(object.get("res_id")));
                    if (server.write(object.getAppClass(), ids, object.getExternalData())) {
                        object.saveSync();
                        item.saveSync(Integer.parseInt(object.get("res_id")));
                    }
                }
            }


        }
    }

    public void syncRead() throws UserException {
        for (Base item: reads) {
            List<Integer> ids = server.search(item.getAppClass(), item.getListConstraints());
            List<String> fields = new ArrayList<>(Arrays.asList(item.getExternalFields()));
            List<Map<String,Object>> result = server.read(item.getAppClass(), ids, fields);
            for (Map<String,Object> entry: result) {
                processEntry(item, entry);
            }
            processDelete(item, ids);
        }
    }

    public boolean processEntry(Base object, Map<String, Object> entry) {
        boolean result = false;
        // Cria um novo objeto para não haver conflito
        Base item = Base.getObjectByAppName(object.getAppClass());
        // Procura se existe algum objeto com o id
        Integer id = (Integer) entry.get("id");
        if (id == null || item == null) return false;
        int count = item.countWhere("res_id = " + id.toString(), new String[]{});
        if (count == 0) {
            // Esse caso é que o registro foi criado externamente, então cria um novo registro local
            item.set("res_id", id.toString());
            for (String field: object.getExternalFields()) {
                itemSet(field, entry.get(field), item);
                /* Se no final do campo houver _id então efetuamos um tratamento especial. Verificamos se existe na tabela um campo com o mesmo nome mas terminado por name
                Nesse caso, atribuimos o valor para ele para que haja uma ordenação mais efetiva
                 */
                if (field.matches("[a-zA-Z_0-9-]+?_id$")) {
                    String new_field = field.replaceFirst("_id", "_id_name");
                    if (object.hasField(new_field)) {
                        item.set(new_field, Tools.getString(entry.get(field), true));
                    }
                }
            }
            item.setLocal(false);
            item.setDraft(false);
            if (item.insert() > 0) result = true;
            item.saveSync();
        } else {
            // Altera o registro
            if (item.load("res_id", id.toString())) {
                for (String field: object.getExternalFields()) {
                    // Não posso alterar o id. Então se o field for id, não muda.
                    if (field.equals("id")) continue;
                    //item.set(field, Tools.getString(entry.get(field)));
                    itemSet(field, entry.get(field), item);
                    if (field.matches("[a-zA-Z_0-9-]+?_id$")) {
                        String new_field = field.replaceFirst("_id", "_id_name");
                        if (object.hasField(new_field)) {
                            item.set(new_field, Tools.getString(entry.get(field), true));
                        }
                    }
                }
                item.update();
                item.saveSync();
                result = true;
            }
        }
        return result;
    }

    public int processDelete(Base object, List<Integer> ids) {
        String compare = "(";
        for (Integer id: ids) {
            if (id != ids.get(0)) compare += ","; // Acrescenta a vírgula para os que não são primeiro item
            compare += id.toString();
        }
        compare += ")";
        List<String> idsToExclude = object.idsList("sys_local = 0 AND res_id IS NOT NULL AND res_id NOT IN " + compare, new String[]{});
        if (idsToExclude.isEmpty()) return 0;
        object.deleteIds(idsToExclude); // Exclui efetivamente os ids que não têm mais associação
        return idsToExclude.size();
    }

    public void itemSet(String field, Object value, Base item) {
        String value_final = Tools.getString(value);
        switch (item.getFieldType(field)) {
            case "DATE":
            case "DATETIME":
                if (value_final.equals("0") || value_final.equals("1"))
                    value_final = null;
        }
        item.set(field, value_final);
    }


}
