package br.com.gabrieloliveira.saleopenerp.view;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.text.InputType;
import android.text.Layout;
import android.view.View;
import android.support.v7.widget.GridLayout;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.gabrieloliveira.saleopenerp.R;
import br.com.gabrieloliveira.saleopenerp.SaleOpenerp;
import br.com.gabrieloliveira.saleopenerp.data.Base;
import br.com.gabrieloliveira.saleopenerp.exception.UserException;
import br.com.gabrieloliveira.saleopenerp.lib.Auth;
import br.com.gabrieloliveira.saleopenerp.lib.Draw;
import br.com.gabrieloliveira.saleopenerp.lib.Tools;

/**
 * Created by Gabriel on 26/01/2016.
 */
public class ListView extends BaseView {

    protected GridLayout gridView;
    protected String[] params;
    protected String orderBy;

    public void show(final GridLayout view, String... params) {
        new ListViewTask(view, params).execute();
    }
    public void show(GridLayout view) {
        show(view, this.params);
    }

    public void show(final GridLayout view, Boolean execute, String... params) {
        if (view == null) return; // Não executa nada caso não tenha passado um gridlayout válido
        Activity activity = (Activity) context;
        gridView = view;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.removeAllViews();
            }
        });
        addSearchView();
        final List<Base> items;
        if (params != null && params.length > 0) {
            this.params = params;
            if (this.object.getSearchType().equals("date_range") && params.length == 2) {
                if (orderBy != null && !orderBy.isEmpty()) {
                    items = this.object.search(params[0], params[1], orderBy);
                } else {
                    items = this.object.search(params[0], params[1]);
                }
            } else {
                if (orderBy != null && !orderBy.isEmpty()) {
                    items = this.object.search(params[0], orderBy, new ArrayList<String[]>());
                } else {
                    items = this.object.search(params[0]);
                }
            }
        } else {
            if (orderBy != null && !orderBy.isEmpty()) {
                items = this.object.list(null, null, orderBy);
            } else {
                items = this.object.list(); // Lista todos
            }
        }
        if (items.isEmpty()) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    view.setRowCount(1);
                    view.setColumnCount(1);
                    Draw.addViewInGrid(view, Draw.getTextView(context, "Nenhum resultado"), 0, 0);
                }
            });

        } else {
            ArrayList<String> totals = this.object.getListTotals();
            int rowCount = items.size()+1;
            if (!totals.isEmpty()) rowCount++;
            final ArrayList<String> fields_list = this.object.getFieldsList();
            final int rowCount_t = rowCount;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    view.setRowCount(rowCount_t);
                    view.setColumnCount(fields_list.size());
                }
            });
            int line = 0;
            int column = 0;
            // Adiciona primeiro o header
            for (String field: fields_list) {
                addElement(view, items.get(0).getLabel(field), line, column, field, items.get(0), true);
                column++;
            }
            line++;
            if (fields_list == null) return;
            HashMap<String, Double> totals_values = new HashMap<>();
            for (final Base item: items) {
                column = 0;
                for (final String field: fields_list) {
                    final int line_t = line;
                    final int column_t = column;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            addElement(view, item.getListValue(field), line_t, column_t, field, item);
                        }
                    });
                    // Verifica se o item está nos totais e soma os valores
                    if (totals.contains(field)) {
                        totals_values.put(field, _sum(totals_values.get(field), item.get(field)));
                    }
                    column++;
                }
                line++;
            }

            if (totals_values.isEmpty()) return;
            // Agora escreve mais uma linha com os totais
            column = 0;
            for (final String field: fields_list) {
                if (totals_values.containsKey(field)) {
                    final Double value = totals_values.get(field);// / (line - 1);
                    final int line_t = line;
                    final int column_t = column;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            addElement(view, items.get(0).getListTotalValue(field, value), line_t, column_t, "", items.get(0));
                        }
                    });
                } else {
                    final int line_t = line;
                    final int column_t = column;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            addElement(view, "", line_t, column_t, "", items.get(0));
                        }
                    });
                }
                column++;
            }
        }
    }

    protected Double _sum(Double current, String value) {
        try {
            Double new_value = Tools.parseDouble(value);
            if (current != null) {
                return new_value + current;
            }
            return new_value;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (current == null) return 0.0;
        return current;
    }

    public void addElement(GridLayout view, String text, int line, int column, String field, Base item) {
        addElement(view, text, line, column, field, item, false);
    }
    public void addElement(final GridLayout view, String text, int line, int column, final String field, Base item, boolean isHeader) {
        String[] colors = new String[]{"#CCCCCC", "#FFFFFF"};
        String color = colors[line % 2];
        final TextView txt = Draw.getTextView(this.context, text);
        txt.setHorizontallyScrolling(false);
        final GridLayout.LayoutParams params = Draw.gridLayoutParams(line, column);
        params.width = 0;
        txt.setBackgroundColor(Color.parseColor(color));
        txt.getLayoutParams().width = 0;
        txt.setPadding(10, 10, 10, 10);
        txt.setClickable(true);
        if (isHeader) {
            // Adiciona o evento para ordenar
            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String order_field = object.getOrderField(field);
                    if (orderBy == null) {
                        orderBy = order_field + " ASC";
                    } else {
                        if (orderBy.equals(order_field + " ASC")) orderBy = order_field + " DESC";
                        else orderBy = order_field + " ASC";
                    }
                    show(view);
                }
            });
        } else {
            object.addClickListener(context, gridView, field, item, txt, searchContainer);
        }
        // Addviewingrid sempre em uma OnRunInrhead
        Activity activity = (Activity) context;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Draw.addViewInGrid(view, txt, params);
                view.requestFocus();
            }
        });
    }

    public void addSearchView() {
        if (searchContainer != null) {
            Activity activity = (Activity) context;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    searchContainer.removeAllViews();
                    switch (object.getSearchType()) {
                        case "date":
                        case "text":
                            addTextSearchView();
                            break;
                        case "date_range":
                            addDateRangeSearchView();
                            break;
                    }
                }
            });
        }
    }

    public void addTextSearchView() {
        final EditText textSearch = new EditText(context);
        textSearch.setHint("Pesquisar...");
        GridLayout.LayoutParams params = Draw.gridLayoutParams(0, 0, 1, 4);
        params.width = 0;
        Draw.addViewInGrid(searchContainer, textSearch, params);
        Button button = new Button(context);
        button.setText("OK");
        button.setPadding(10, 10, 10, 10);
        params = Draw.gridLayoutParams(0, 1, 1, 1);
        if (this.params != null && this.params.length > 0) {
            textSearch.setText(this.params[0]);
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Efetua a busca.
                show(gridView, textSearch.getText().toString());
            }
        });
        Draw.addViewInGrid(searchContainer, button, params);
    }

    public void addDateRangeSearchView() {
        final EditText textDateFrom = new EditText(context);
        final EditText textDateTo = new EditText(context);
        textDateFrom.setHint("De:");
        textDateTo.setHint("Até:");
        textDateFrom.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
        textDateTo.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
        GridLayout.LayoutParams params = Draw.gridLayoutParams(0, 0, 1, 3);
        params.width = 0;
        Draw.addViewInGrid(searchContainer, textDateFrom, params);
        params = Draw.gridLayoutParams(0, 1, 1, 3);
        params.width = 0;
        Draw.addViewInGrid(searchContainer, textDateTo, params);
        Button button = new Button(context);
        button.setText("OK");
        button.setPadding(10, 10, 10, 10);
        params = Draw.gridLayoutParams(0, 2, 1, 1);
        final DatePickerDialogView fromView = new DatePickerDialogView(context, textDateFrom);
        final DatePickerDialogView toView = new DatePickerDialogView(context, textDateTo);
        // Recupera os dados que foram enviados
        if (this.params != null && this.params.length == 2) {
            fromView.setTextDate(fromView.convertDateToView(this.params[0]));
            toView.setTextDate(toView.convertDateToView(this.params[1])); // Converte a data para visualização
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Efetua a busca.
                if (fromView.is_set() && toView.is_set())
                    show(gridView, fromView.getDateStringCompare(), toView.getDateStringCompare());
            }
        });
        Draw.addViewInGrid(searchContainer, button, params);

    }

    public String getStringDatePicker(DatePicker datePicker) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(new Date(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth()));
    }

    protected ProgressDialog progressDialog;
    protected class ListViewTask extends AsyncTask<Void, Integer, Boolean> {
        protected String error_msg;
        protected GridLayout view_param;
        protected String[] params_param;
        ListViewTask(GridLayout view, String... params) {
            view_param = view;
            params_param = params;
        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(R.string.app_name);
            progressDialog.setMessage(context.getResources().getString(R.string.message_list_loading));
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... paramsn) {
            show(view_param, true, params_param);
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
        protected Intent mServiceIntent;
        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.dismiss();
            // Não faz nada. Já adicionou os controles
        }
    }

}
