package br.com.gabrieloliveira.saleopenerp.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Gabriel on 30/01/2016.
 */
public class DatePickerDialogView {
    protected int day = -1;
    protected int month = -1;
    protected int year = -1;
    protected static final int DATE_DIALOG_ID = 0;
    protected TextView textView;

    protected Context context;

    public int getDay() {
        return day;
    }
    public int getMonth() {
        return month;
    }
    public int getYear() {
        return year;
    }

    DatePickerDialogView(Context context, TextView textView) {
        this.textView = textView;
        this.context = context;
        init();
    }

    protected void init() {
        textView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    showDialog();
            }
        });
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener =
        new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int yearParam,
                                  int monthOfYear, int dayOfMonth) {
                year = yearParam;
                month = monthOfYear;
                day = dayOfMonth;
                updateDisplay();
            }
        };

    private void updateDisplay() {
        this.textView.setText(getDateStringFormat());
    }
    protected void showDialog() {
        final Calendar c = Calendar.getInstance(); // Para calcular a data atual, caso não esteja definida

        int dYear = year > 0 ? year : c.get(Calendar.YEAR);
        int dMonth = month > 0 ? month : c.get(Calendar.MONTH);
        int dDay = day > 0 ? day : c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(context,
                mDateSetListener,
                dYear, dMonth, dDay);
        dialog.show();
    }

    public String getDateStringCompare() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(new Date(year - 1900, month, day));
    }

    public String getDateStringFormat() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return simpleDateFormat.format(new Date(year - 1900, month, day)); // A classe Date sempre soma 1900 ao ano
    }

    public String convertDateToView(String dateDatabase) {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateDatabase);
            return new SimpleDateFormat("dd/MM/yyyy").format(date);
        } catch (ParseException e) {
            return dateDatabase;
        }
    }
    public void setTextDate(String date) {
        this.textView.setText(date);
        // Define também o dia, mês e ano
        try {
            Date oDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            day = oDate.getDay();
            month = oDate.getMonth();
            year = oDate.getYear() + 1900;
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public boolean is_set() {
        if (day >= 0 && month >= 0 && year >= 0) {
            return true;
        }
        return false;
    }
}
