package br.com.gabrieloliveira.saleopenerp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.support.v7.widget.GridLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.gabrieloliveira.saleopenerp.SaleOpenerp;
import br.com.gabrieloliveira.saleopenerp.lib.Tools;

/**
 * Created by Gabriel on 14/10/2015.
 * Todos as classes derivadas deverão possuir os campos de sincronia. Que são
 */
public class Base {
    protected Map<String, String> fields;
    protected String TABLE_NAME;

    protected static SQLiteDatabase db;
    protected static SQLiteDatabase db_write;
    protected static DataOpenHelper helper;
    /**
     * Mapa com a associação Field_name > Field_type - Alterado para que seja possível criar campos base para todos os registros. Dessa forma a string CREATE Não é mais necessária
     */
    protected Map<String, String> FIELDS;
    protected String APP_OBJECT = "";
    protected String[] FIELDS_SEARCH = {};
    protected String DISPLAY_NAME = "";
    protected boolean INCLUDE_SEARCH = true;
    protected String[] EXTERNAL_FIELDS;
    protected Map<String, String> relations;
    protected ArrayList<String> fields_list;
    protected HashMap<String, String> labels;
    protected ArrayList<String[]> list_constraints;
    protected String[] search_types = new String[]{"text", "date_range", "date"};
    protected String search_type = "text"; // O padrão é busca por nome ou outros campos do array FIELDS_SEARCH
    protected ArrayList<String> list_totals;
    protected ArrayList<String> form_fields;
    protected String relation_list_name = "name";
    protected int sync_priority = 0;

    Base() {
        this.fields = new HashMap<>();
        helper = DataOpenHelper.getInstance(SaleOpenerp.getContext());
    }

    public String getDefaultOrder() {
        return "id";
    }

    /**
     * Adiciona a definição de campos padrão para todos as classes que herdarem. Para isso, as subclasses devem chamar o método init no seu construtor
     */
    public void init() {
        addFieldDefinition("id", "INTEGER PRIMARY KEY");
        addFieldDefinition("res_id", "INTEGER NULL DEFAULT NULL"); // O id do registro no openerp
        addFieldDefinition("sync_date", "DATETIME NULL DEFAULT NULL");
        addFieldDefinition("sync_id", "INTEGER NULL DEFAULT NULL");
        addFieldDefinition("create_date", "DATETIME");
        addFieldDefinition("sys_local", "BOOLEAN DEFAULT '0'");
        addFieldDefinition("sys_draft", "BOOLEAN DEFAULT '0'"); // Esse campo indica se o registro criado é temporário. Por padrão ao iniciar um novo registro em draft o registro antigo será excluído assim como suas relações locais
    }

    public void saveSync() {
        saveSync(null);
    }

    public void saveSync(Integer id) {
        this.set("sync_date", "NOW()");
        if (id != null)
            this.set("res_id", id.toString());
        this.update();
    }

    Base(String id) {
        find(id);
    }

    public ArrayList<String> getFieldsList() {
        return fields_list;
    }

    /**
     * Usar ao iniciar uma transação. Para fazer o commit, chamar sucessfulTransaction e endTransaction
     * Toda transação deve ser terminada e por isso o ideal é colocar endTransaction em um bloco finally.
     * Se a transação for terminada sem sucessfulTransaction então é feito um rollback
     */
    public static void beginTransaction() {
        if (helper == null) helper = DataOpenHelper.getInstance(SaleOpenerp.getContext());
        getDbWrite().beginTransaction();
    }

    /**
     * Deve ser chamada para uma transação ser terminada com commit. Se não for chamada uma transação iniciada com beginTransaction ficará em aberto.
     * Ou se chamado endTransaction sem esse método, será feito o rollback.
     */
    public static void successfulTransaction() {
        if (helper == null) helper = DataOpenHelper.getInstance(SaleOpenerp.getContext());
        getDbWrite().setTransactionSuccessful();
    }

    /**
     * Finaliza a transação. Esse método deve ser chamado SEMPRE, caso contrário a transação ficará aberta e nada será efetivamente guardado no banco de dados.
     * Se chamada sem sucessfulTransaction, será feito o rollback
     */
    public static void endTransaction() {
        if (helper == null) helper = DataOpenHelper.getInstance(SaleOpenerp.getContext());
        getDbWrite().endTransaction();
    }

    protected static SQLiteDatabase getDbWrite() {
        if (db_write == null) db_write = helper.getWritableDatabase();
        return db_write;
    }

    public void insertFromJson(JSONObject json) throws JSONException {
        for (String field: getFields()) {
            if (field == "id")
                this.fields.put("id", json.getString("pk"));
            else
                this.fields.put(field, json.getJSONObject("fields").getString(field));
        }
        this.insert();
    }

    public String getAppClass() {
        return APP_OBJECT;
    }

    protected static SQLiteDatabase getDbRead() {
        if (db == null) db = helper.getReadableDatabase();
        return db;
    }

    public void set(String key, String value) {
        this.fields.put(key, value);
    }

    public String get(String key, String defaultValue) {
        if (this.fields.containsKey(key)) return this.fields.get(key);
        return defaultValue;
    }

    public String get(String key) {
        return this.get(key, null);
    }

    public long insert(ContentValues values) {
        Long ret = this.getDbWrite().insert(getTableNameSql(), "", values);
        // Atribui o id
        set("id", ret.toString());
        return ret;
    }

    public long update(ContentValues values, String where, String[] whereArgs) {
        long ret = this.getDbWrite().update(getTableNameSql(), values, where, whereArgs);
        return ret;
    }

    public long update() {
        ContentValues values = this.getContentValues();
        return this.update(values, "id = ?", new String[]{(String) values.get("id")});
    }

    public long delete(String where, String[] whereArgs) {
        long ret = this.getDbWrite().delete(getTableNameSql(), where, whereArgs);
        return ret;
    }

    public long delete() {
        return this.delete("id = ?", new String[]{get("id")});
    }

    public long deleteAll() {
        return this.delete("", null);
    }

    public long update(String id) {
        ContentValues values = this.getContentValues();
        return this.update(values, "id = ?", new String[]{id});
    }

    public ContentValues getContentValues() {
        // Vamos alterar para que caso haja um campo do tipo DECIMAL, formatemo-os de volta para o normal que é com ponto
        ContentValues values = new ContentValues();
        for (Map.Entry<String, String> entry: FIELDS.entrySet()) {
            String field = entry.getKey();
            String field_type = getFieldType(field);
            if (this.fields.containsKey(field)) {
                if (field_type.equals("DECIMAL") || field_type.equals("FLOAT") || field_type.equals("DOUBLE")) {
                    Double value = Tools.parseDouble(this.fields.get(field));
                    if (value == null)
                        values.put(field, value);
                    else
                        values.put(field, value.toString());
                } else {
                    values.put(field, this.fields.get(field));
                }
            }
        }
        return values;
    }

    public long insert() {
        ContentValues values = this.getContentValues();
        return this.insert(values);
    }

    public Cursor query(String where, String[] whereReplaces, String orderBy) {
        return this.query(getFields(), where, whereReplaces, "id", orderBy, null, null);
    }

    public Cursor query(String where, String[] whereReplaces, String orderBy, String limit) {
        return this.query(getFields(), where, whereReplaces, "id", orderBy, "", limit);
    }
    public Cursor query() {
        return this.query("");
    }

    public Cursor query(String where, String[] whereReplaces) {
        return this.query(getFields(), where, whereReplaces, "id", getDefaultOrder(), "", null);
    }
    public Cursor query(String where) {
        return this.query(where, null);
    }

    public Cursor query(String[] fields, String where, String[] whereReplaces, String groupBy, String orderBy, String having, String limit) {
        Cursor ret = this.getDbRead().query(getTableNameSql(), fields, where, whereReplaces, groupBy, having, orderBy, limit);
        return ret;
    }

    public Cursor rawQuery(String sql, String[] selectionArgs) {
        Cursor ret = this.getDbRead().rawQuery(sql, selectionArgs);
        return ret;
    }

    public boolean find(String id) {
        if (id == null || id.isEmpty()) return false;
        return load("id", id);
    }

    public boolean findByRes(String res_id) {
        if (res_id == null || res_id.isEmpty()) return false;
        return load("res_id", res_id);
    }

    public boolean loaded = false;
    public boolean load(String name, String value) {
        String[] replaces = {value};
        Cursor c = this.query(name + " = ?", replaces, name, "1");
        if (c.moveToFirst()) {
            for (String field: getFields()) {
                this.fields.put(field, c.getString(c.getColumnIndex(field)));
            }
            loaded = true;
            c.close();
            return true;
        }
        c.close();
        return false;
    }

    public boolean has(String id) {
        String[] replaces = {id};
        Cursor c = this.query("id = ?", replaces, "id", "1");
        if (c.moveToFirst()) {
            c.close();
            return true;
        }
        c.close();
        return false;
    }

    public List<Base> list() {
        return this.list("", new String[]{});
    }

    protected String constraintWhere;
    protected String[] constraintWhereReplaces;

    protected ArrayList<String[]> constraints;
    public void addConstraint(String... constraint) {
        if (constraint.length != 3) return;
        if (constraints == null) constraints = new ArrayList<>();
        constraints.add(constraint);
        resolveConstraints(constraints);
    }
    public ArrayList<String[]> getRelatedConstraints(String field) {
        return null;
    }
    protected void clearConstraints() {
        if (constraints != null)
            constraints.clear();
    }
    protected void resolveConstraints(ArrayList<String[]> constraints) {
        String where = "";
        String[] whereReplaces = new String[constraints.size()];
        int index = 0;
        for (String[] constraint: constraints) {
            // Todas as constraints devem possuir 3 posições. CAMPO -> OPERADOR -> VALOR
            where += constraint[0] + " " + constraint[1] + " ?";
            whereReplaces[index] = constraint[2];
            index++;
            if (index != constraints.size()) {
                where += " AND ";
            }
        }
        constraintWhere = where;
        constraintWhereReplaces = whereReplaces;
    }
    public List<Base> list(ArrayList<String[]> constraints) {
        resolveConstraints(constraints);
        return list(constraintWhere, constraintWhereReplaces);
    }
    public List<Base> list(String where, String[] whereReplaces) {
        ArrayList<Object> list = getWhereWithConstraints(where, whereReplaces);
        Cursor c;
        try {
            c = this.query((String) list.get(0), (String[]) list.get(1));
        } catch (ClassCastException e) {
            e.printStackTrace();
            c = this.query(where, whereReplaces);
        }
        return _list(c);
    }

    public List<Base> list(String where, String[] whereReplaces, String orderBy) {
        ArrayList<Object> list = getWhereWithConstraints(where, whereReplaces);
        Cursor c;
        try {
            c = this.query((String)list.get(0), (String[])list.get(1), orderBy);
        } catch (ClassCastException e) {
            e.printStackTrace();
            c = this.query(where, whereReplaces, orderBy);
        }
        return _list(c);
    }

    public int count(String sql, String[] replaces) {
        int result = 0;
        Cursor c = rawQuery(sql, replaces);
        if (c.moveToFirst()) {
            result = c.getInt(0);
        }
        return result;
    }

    public int count(String sql) {
        return this.count(sql, null);
    }

    public int countWhere(String where, String[] replaces) {
        int result = 0;
        String sql = "SELECT COUNT(*) FROM `" + TABLE_NAME + "` WHERE " + where;
        return count(sql, replaces);
    }

    public int countWhere(String where) {
        return countWhere(where, null);
    }

    protected List<Base> _list(Cursor c) {
        List<Base> list = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                Base object = Base.getObject(this.getClass().getSimpleName());
                for (Map.Entry<String, String> entry: FIELDS.entrySet()) {
                    String field = entry.getKey();
                    object.set(field, c.getString(c.getColumnIndex(field)));
                }
                list.add(object);
            } while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public List<Base> rawList(String sql, String[] replaces) {
        Cursor c = rawQuery(sql, replaces);
        return _list(c);
    }

    public List<Base> rawList(String sql) {
        Cursor c = rawQuery(sql, null);
        return _list(c);
    }

    public static List<List<Base>> searchAll(String query) {
        Map<String, Base> objects = DataOpenHelper.getObjects();
        Iterator i = objects.entrySet().iterator();
        List<List<Base>> list = new ArrayList<>();
        while (i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();
            String key = (String) entry.getKey();
            Base object = (Base) entry.getValue();
            if (object.searchable())
                list.add(object.search(query));
        }
        return list;
    }

    public List<Base> search(String query) {
        return search(query, null, new ArrayList<String[]>());
    }

    public List<Base> search(String date_from, String date_to) {
        return search(date_from, date_to, "");
    }
    /**
     * Pesquisa para o tipo de busca de date_range
     * @param date_from
     * @param date_to
     * @return
     */
    public List<Base> search(String date_from, String date_to, String orderBy) {
        String where = "";
        String[] whereReplaces = new String[getFieldsSearch().length * 2]; // É duplicado pois cada campo vai comparar duas vezes
        int inicial = 0;
        int index = 0;
        for (String field: getFieldsSearch()) {
            if (inicial > 0)
                where += " OR ";
            where += "(" + field + " >= ? AND " + field + " <= ?" + ")";
            whereReplaces[index] = date_from;
            index++;
            whereReplaces[index] = date_to;
            inicial++;
            index++;
        }
        if (orderBy.isEmpty()) orderBy = null;
        return list(where, whereReplaces, orderBy);
    }

    /**
     * @deprecated Since 2016-01-30. Esse método não será mais usado, já que a própria classe se encarregará de definir as constraints
     * Atalho para não passar OrderBy
     * @param query
     * @param constraints
     * @return
     */
    public List<Base> search(String query, ArrayList<String[]> constraints) {
        return search(query, null, constraints);
    }

    /**
     * Monta uma sql para pesquisa sempre utilizando o OR, já que equivale a dizer que qualquer um dos campos em FIELDS_SEARCH sejam selecionados ele retornará registros correspondentes
     * @param query
     * @param orderBy
     * @param constraints
     * @return
     */
    public List<Base> search(String query, String orderBy, ArrayList<String[]> constraints) {
        if (constraints != null) resolveConstraints(constraints);
        String sql = "";
        String[] replaces = new String[this.getFieldsSearch().length];
        int inicial = 0;
        for (String field: this.getFieldsSearch()) {
            if (inicial > 0)
                sql += " OR ";
            if (getSearchType().equals("text"))
                sql += field + " LIKE ?";
            else
                sql += field + " = ?";
            replaces[inicial] = "%" + query + "%";
            inicial++;
        }
        List<Base> list;
        if (orderBy == null || orderBy.isEmpty()) {
            list = this.list(sql, replaces);
        } else {
            list = this.list(sql, replaces, orderBy);
        }
        return list;
    }

    /**
     * Sempre Retorna um array list com 2 posições, a primeira (0) é a String where e a segunda o array String[] whereReplaces
     * @param where
     * @param whereReplaces
     * @return
     */
    protected ArrayList<Object> getWhereWithConstraints(String where, String[] whereReplaces) {
        ArrayList<Object> list = new ArrayList<>();
        if (constraintWhere != null) {
            System.out.println("WHERE CONSRAINT");
            System.out.println(constraintWhere);
            if (where == null || where.isEmpty()) {
                list.add(constraintWhere);
                list.add(constraintWhereReplaces);
                return list;
            }
            if (!constraintWhere.equals(""))
                where = "(" + where + ") AND (" + constraintWhere + ")";
            String[] newReplaces = new String[whereReplaces.length + constraintWhereReplaces.length];
            int index = 0;
            for (String replace : whereReplaces) {
                newReplaces[index] = replace;
                index++;
            }
            for (String replace : constraintWhereReplaces) {
                newReplaces[index] = replace;
                index++;
            }
            whereReplaces = newReplaces;
        }
        list.add(where);
        list.add(whereReplaces);
        return list;
    }

    /**
     * Cria a sql de create para o banco de dados utilizando o hashmap FIELDS que possue a associação de nome e tipo
     * @return
     */
    public String getCreateSql() {
        // Monta a SQL de Criação
        String sql = "CREATE TABLE `" + TABLE_NAME + "` (";
        int i = 1;
        int fields_size = FIELDS.size();
        for (Map.Entry<String, String> entry: FIELDS.entrySet()) {
            String name = entry.getKey();
            String type = entry.getValue();
            sql += name + " " + type;
            if (i != fields_size) sql += ", ";
            i++;
        }
        sql += ");";
        return sql;
    }

    public String getUpdateSql(Map<String, String> fields) {
        /*
        Iterator iterator = fields.entrySet().iterator();
        // Comparamos cada campo com os que já estão no objeto
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) iterator.next();
            String field = entry.getKey();
            String type = entry.getValue();
            // Se o campo não estiver em nossos registros iremos deletá-lo. Se estiver e o tipo for diferente, alteramos

        }*/
        return ""; // Atualmente não usada, o sqlite não suporta a alteração de campos de tabela
    }

    public String[] getFields() {
        String[] fields = new String[FIELDS.size()];
        int i = 0;
        for (String field: FIELDS.keySet()) {
            fields[i] = field;
            i++;
        }
        return fields;
    }

    public String getTableName() {return TABLE_NAME;}

    /**
     * Retorna o nome da tabela circundada com `` para evitar conflito de nomes com nomes reservados pelo SQLite
     * @return
     */
    public String getTableNameSql() {return "`" + TABLE_NAME + "`";}

    public String[] getFieldsSearch() {
        if (this.FIELDS_SEARCH.length == 0) {
            return new String[]{"name"};
        }
        return this.FIELDS_SEARCH;
    }

    public String getDisplayName() {
        return this.DISPLAY_NAME;
    }

    public boolean searchable() {
        return this.INCLUDE_SEARCH;
    }

    public List<String> idsList(String where, String[] replaces) {
        Cursor c = query(new String[]{"id"}, where, replaces, null, "id", null, null);
        List<String> list = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                list.add(c.getString(c.getColumnIndex("id")));
            } while (c.moveToNext());
        }
        c.close();
        return list;
    }


    /**
     * @param className
     * @return
     */
    public static Base getObject(String className) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName("br.com.gabrieloliveira.saleopenerp.data." + className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            return (Base) clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            System.out.println(className);
            e.printStackTrace();
        }
        return null;
    }

    public static Base getObjectByAppName(String appname) {
        Map<String, Base> objects = DataOpenHelper.getObjects();
        Iterator iterator = objects.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Base> entry = (Map.Entry) iterator.next();
            if (entry.getKey().equals(appname)) {
                return getObject(entry.getValue().getClass().getSimpleName());
            }
        }
        return null;
    }

    public List<String> getIds() {
        String sql = "SELECT id FROM " + getTableNameSql();
        List<Base> list = rawList(sql);
        List<String> ids = new ArrayList<>();
        for (Base o: list) {
            ids.add(o.get("id"));
        }
        return ids;
    }

    public void deleteIds(List<String> ids) {
        if (ids != null && !ids.isEmpty()) {
            String where = "";
            for (int i = 0; i < ids.size(); i++) {
                String id = ids.get(i);
                if (i > 0) where += " OR ";
                where += "id = " + id;
            }
            delete(where, null);
        }
    }

    public HashMap<String, Object> getExternalData() {
        HashMap<String, Object> result = new HashMap<>();
        for (String field: EXTERNAL_FIELDS) {
            String value = get(field);
            Object final_value;
            if (value == null) continue; // Não adiciona os que não possuem valor
            switch (getFieldType(field)) {
                case "DATETIME":
                    if (value.equals("0")) continue; // Se for um campo do tipo datetime e ele possuir o valor 0, não adiciona ao dicionário também
                    final_value = value;
                    break;
                case "FLOAT":
                case "DECIMAL":
                case "DOUBLE":
                    try {
                        final_value = Double.parseDouble(value);
                    } catch (NumberFormatException e) {
                        final_value = new Double(0);
                    }
                    break;
                case "INTEGER":
                    try {
                        final_value = Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        final_value = new Integer(0);
                    }
                    break;
                case "BOOLEAN":
                    System.out.println("FIELD: " + field);
                    System.out.println("VALUE: " + value);
                    final_value = new Boolean(value.equals("1"));
                    break;
                default:
                    final_value = value;
            }
            result.put(field, final_value);
        }
        return result;
    }

    public String[] getExternalFields() {
        // Sempre adiciona o campo id
        String[] fields = new String[EXTERNAL_FIELDS.length+1];
        fields[0] = "id";
        for (int i = 1; i < fields.length; i++) {
            fields[i] = EXTERNAL_FIELDS[i-1];
        }
        return fields;
    }

    public ArrayList<String[]> getListConstraints() {
        if (list_constraints == null) list_constraints = new ArrayList<>(); // Nunca retorna null
        return list_constraints;
    }

    public void addListConstraint(String... constraint) {
        if (constraint.length != 3) return; // Se não tem 3 exatas não adiciona
        if (list_constraints == null) list_constraints = new ArrayList<>();
        list_constraints.add(constraint);
    }

    protected void addFieldDefinition(String name, String type) {
        if (FIELDS == null) FIELDS = new HashMap<>();
        FIELDS.put(name, type);
    }

    public String getFieldType(String field) {
        String undefined = "UNDEFINED";
        String[] types = new String[]{"BOOLEAN", "DECIMAL", "VARCHAR", "INTEGER", "TEXT", "FLOAT", "DOUBLE", "CHAR", "DATE", "DATETIME"};
        if (!FIELDS.containsKey(field)) return undefined;
        String value = FIELDS.get(field);
        for (String type: types) {
            if (value.matches(type + ".*")) return type;
        }
        return undefined;
    }

    protected void setFieldList(String... fields) {
        if (fields_list == null) fields_list = new ArrayList<>();
        fields_list.clear();
        addFieldList(fields);
    }
    protected void addFieldList(String... fields) {
        if (fields_list == null) fields_list = new ArrayList<>();
        for (String field: fields) {
            fields_list.add(field);
        }
    }

    protected void addLabel(String field, String label) {
        if (labels == null) labels = new HashMap<>();
        labels.put(field, label);
    }

    public String getLabel(String field) {
        if (labels == null || !labels.containsKey(field)) return "";
        return labels.get(field);
    }

    public HashMap<String, String> getLabels() {
        if (labels == null) labels = new HashMap<>();
        return labels; // Não retorna NULL
    }

    public void addRelation(String field, String object) {
        if (relations == null) relations = new HashMap<>();
        relations.put(field, object);
    }

    public boolean hasRelation(String field) {
        if (relations == null) relations = new HashMap<>();
        return relations.containsKey(field);
    }

    public String getRelation(String field) {
        if (relations == null) relations = new HashMap<>();
        return relations.get(field);
    }

    public int getRelationsSize() {
        if (relations == null) relations = new HashMap<>();
        return relations.size();
    }

    public Base getRelationObject(String field) {
        String name = getRelation(field);
        if (name == null) return null;
        return DataOpenHelper.getObjects().get(name);
    }

    public String getListValue(String field) {
        if (getFieldType(field).equals("BOOLEAN")) {
            String value = get(field);
            if (value.equals("1")) return "Sim";
            return "Não";
        }
        return get(field);
    }

    public String getListTotalValue(String field, Double value) {
        return value.toString();
    }

    public void setSearchType(String search_type) {
        this.search_type = search_type;
    }

    public String getSearchType() {
        return search_type;
    }

    public void setListTotals(String... totals) {
        if (list_totals == null) list_totals = new ArrayList<>();
        for (String total: totals) {
            this.list_totals.add(total);
        }
    }

    public ArrayList<String> getListTotals() {
        if (list_totals == null) list_totals = new ArrayList<>();
        return list_totals;
    }

    public boolean hasField(String field) {
        if (FIELDS == null) return false;
        return FIELDS.containsKey(field);
    }

    public String getOrderField(String field) {
        if (field.matches("[a-zA-Z_0-9-]+?_id$")) {
            String new_name = field.replaceFirst("_id", "_name");
            if (hasField(new_name)) return new_name;
        }
        return field;
    }

    public Map<String, String> getFieldsTypes() {
        return FIELDS;
    }

    public void setFormFields(String... fields) {
        if (form_fields == null) form_fields = new ArrayList<>();
        for (String field: fields) {
            form_fields.add(field);
        }
    }

    public void addFormField(String field) {
        if (form_fields == null) form_fields = new ArrayList<>();
        form_fields.add(field);
    }

    public ArrayList<String> getFormFields() {
        if (form_fields == null) form_fields = new ArrayList<>();
        return form_fields;
    }

    public ArrayList<String> getFormFieldsRelation() {
        return getFormFields();
    }

    public void setRelationListName(String name) {
        relation_list_name = name;
    }

    public String getRelationListName() {
        return relation_list_name;
    }

    public String getRelationListNameValue() {
        return get(getRelationListName());
    }

    public boolean isLocal() {
        if (get("sys_local") == "0") return false;
        return true;
    }

    public boolean isDraft() {
        if (get("sys_draft") == "0") return false;
        return true;
    }

    public void setLocal() {
        set("sys_local", "1");
    }

    public void setLocal(boolean value) {
        if (value) setLocal();
        else set("sys_local", "0");
    }

    public void setDraft() {
        set("sys_draft", "1");
    }

    public void setDraft(boolean value) {
        if (value) setDraft();
        else set("sys_draft", "0");
    }

    /**
     * Retorna um Map de objetos de relação one2many. Ou seja, para serem incluídos objetos desse tipo no objeto corrente.
     * Isso significa que os objetos devem possuir um campo de relação que é a chave e o objeto o valor
     * @return
     */
    public Map<String, String> getFormRelationObjects() {
        return new HashMap<String, String>();
    }

    public ArrayList<String> getFormFieldsList() {
        return new ArrayList<>();
    }

    public List<String> getOnChangeFields() {
        return new ArrayList<String>();
    }

    /**
     * Retorna um dicionário com os valores campos -> valores a serem alterados no formulário
     * @return
     */
    public HashMap<String, String> processOnchange(String field, String value, HashMap<String, String> values) {
        return new HashMap<>();
    }

    public void updateAfterRelationsSave() {
        return;
    }

    /**
     * Cria um registro na tabela sync para ser sincronizado e criado no OpenERP. Somente cria se o registro estiver com a flag "local".
     * @param id
     */
    public void createSyncCreateEntry(String id) {
        if (isLocal()) {
            System.out.println("########## Criando Entrada de CRIAÇAO PARA O ID: " + id);
            Sync sync = new Sync();
            sync.set("sync", "0");
            sync.set("is_create", "1");
            sync.set("resource", this.getAppClass());
            sync.set("sync_priority", new Integer(getSyncPriority()).toString());
            sync.set("res_id", id);
            sync.set("local_id", id);
            sync.insert();
        }
    }

    public void createSyncUpdateEntry(String id) {
        System.out.println("########## Criando Entrada de UPDATE para o ID: " + id);
        Sync sync = new Sync();
        sync.set("sync", "0");
        sync.set("is_create", "0");
        sync.set("resource", this.getAppClass());
        sync.set("sync_priority", new Integer(getSyncPriority()).toString());
        sync.set("res_id", id);
        sync.set("local_id", get("id"));
        sync.insert();
    }

    public void createSyncUpdateEntry() {
        createSyncUpdateEntry(get("res_id"));
    }

    public void createSyncDeleteEntry(String id) {
        System.out.println("########## Criando Entrada de DELECAO para o ID: " + id);
        Sync sync = new Sync();
        sync.set("sync", "0");
        sync.set("is_create", "0");
        sync.set("is_delete", "1");
        sync.set("resource", this.getAppClass());
        sync.set("sync_priority", new Integer(getSyncPriority()).toString());
        sync.set("res_id", id);
        sync.set("local_id", get("id"));
        sync.insert();
    }

    public void setSyncPriority(int value) {
        sync_priority = value;
    }

    public int getSyncPriority() {
        return sync_priority;
    }

    protected HashMap<String, String> deppends = new HashMap<>();
    public HashMap<String, String> getDeppends() {
        return deppends;
    }

    /**
     * Adiciona uma dependência para algum campo específico. É importante lembrar no entando que é preciso setar o campo sync_priority corretamente
     * para que na sincronização, o registro relacionado já tenha sido sincronizado. Então dessa forma é possível pegar os novos valores.
     * @param key
     * @param value
     */
    public void addDeppend(String key, String value) {
        deppends.put(key, value);
    }

    public boolean hasDeppends() {
        return !deppends.isEmpty();
    }

    protected List<String> form_fields_required = new ArrayList<>();
    public List<String> getFormFieldsRequired() {
        return form_fields_required;
    }

    public void setFormFieldsRequired(String... fields) {
        for (String field: fields) {
            form_fields_required.add(field);
        }
    }

    protected String form_field_required = "";

    public String getFormFieldRequiredError() {
        return form_field_required;
    }

    protected String form_field_unique_error = "";
    public void setFormFieldUniqueError(String value) {
        form_field_unique_error = value;
    }
    public String getFormFieldUniqueError() {
        return form_field_unique_error;
    }

    protected List<String[]> form_fields_unique = new ArrayList<String[]>();

    public void addFormFieldUnique(String... fields) {
        form_fields_unique.add(fields);
    }

    public List<String[]> getFormFieldsUnique() {
        return form_fields_unique;
    }

    protected List<String> form_fields_readonly;
    public void setFormFieldsReadOnly(String... fields) {
        form_fields_readonly = new ArrayList<String>();
        for (String field: fields) {
            form_fields_readonly.add(field);
        }
    }

    public List<String> getFormFieldsReadOnly() {
        if (form_fields_readonly == null) form_fields_readonly = new ArrayList<String>();
        return form_fields_readonly;
    }

    public boolean isFormReadOnly(String field) {
        return getFormFieldsReadOnly().contains(field);
    }

    public boolean validate() {
        // Método verifica os valores de campos. Atualmente apenas verifica se campos no dict form_fields_required estão com valor
        for (String field: form_fields_required) {
            String value = get(field);
            form_field_required = field;
            if (value == null || value.equals("")) {
                return false;
            }
        }
        return true;
    }

    protected String validate_message = "";
    public String getValidateMessage() {
        return validate_message;
    }

    public void setValidateMessage(String message) {
        validate_message = message;
    }

    protected String validate_error_type = "required";
    public String getValidateErrorType() {
        return validate_error_type;
    }

    public void setValidateErrorType(String type) {
        validate_error_type = type;
    }

    protected String validate_field_error;
    public String getValidateFieldError() {
        return validate_field_error;
    }

    public void setValidateFieldError(String field) {
        validate_field_error = field;
    }

    public void addClickListener(Context context, GridLayout gridLayout, String field, Base item, View v, GridLayout gridSearch) {

    }

    public void setLoaded() {
        loaded = true;
    }

    /**
     * Retorna uma lista para os itens que devem ser colocados ao final de uma lista de relações em um formulário.
     * Se não houver, deve retornar null.
     * @param data
     * @return
     */
    public List<String> getColumnsRelationTotals(List<HashMap<String, String>> data) {
        return null;
    }

    protected HashMap<String, HashMap<String, String>> form_fields_options = new HashMap<>();
    public void addFormFieldOption(String field, String key, String value) {
        if (form_fields_options.get(field) == null) form_fields_options.put(field, new HashMap<String, String>());
        form_fields_options.get(field).put(key, value);
    }

    public boolean hasFormFieldOptions(String field) {
        return form_fields_options.get(field) != null && !form_fields_options.isEmpty();
    }
    public HashMap<String, String> getFormFieldOptions(String field) {
        if (form_fields_options.get(field) == null) form_fields_options.put(field, new HashMap<String, String>());
        return form_fields_options.get(field);
    }

    public Integer getFormPositionRedirect() {
        return null;
    }

    public Integer getViewPositionRedirect() {
        return null;
    }

    protected ArrayList<String> field_currency_format = new ArrayList<>();
    public void setFieldsCurrencyFormat(String... fields) {
        for (String item: fields) {
            field_currency_format.add(item);
        }
    }
    public boolean isCurrency(String field) {
        return field_currency_format.contains(field);
    }

    public Map<String, String> getData() {
        return fields;
    }

    public void traceData() {
        Iterator iterator = fields.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> item = (Map.Entry) iterator.next();
            System.out.println(item.getKey() + " = " + item.getValue());
        }
    }
}
