package br.com.gabrieloliveira.saleopenerp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.gabrieloliveira.saleopenerp.exception.UserException;
import br.com.gabrieloliveira.saleopenerp.lib.Auth;
import br.com.gabrieloliveira.saleopenerp.lib.BaseActivity;
import br.com.gabrieloliveira.saleopenerp.lib.Server;
import br.com.gabrieloliveira.saleopenerp.lib.Sync;

public class LoginActivity extends BaseActivity {

    protected EditText textUser;
    protected EditText textPassword;
    protected Button buttonLogin;

    protected String user;
    protected String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (Auth.instance().isLogged()) {
            // Se o usuário já estiver logado, redireciona para a tela principal
            startActivity(MainActivity.class);
        }
        init();
    }

    protected void init() {
        textUser = (EditText) findViewById(R.id.txt_login_user);
        textPassword = (EditText) findViewById(R.id.txt_login_password);
        buttonLogin = (Button) findViewById(R.id.btn_login_login);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attrib();
                if (validate())
                    new LoginViewTask(user, password).execute();
            }
        });
    }

    protected void attrib() {
        user = textUser.getText().toString();
        password = textPassword.getText().toString();
    }

    protected boolean validate() {
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    protected ProgressDialog progressDialog;
    protected class LoginViewTask extends AsyncTask<Void, Integer, Boolean> {
        protected final String user;
        protected final String password;

        protected String error_msg;

        public LoginViewTask(String user, String password) {
            this.user = user;
            this.password = password;
        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setTitle(R.string.app_name);
            progressDialog.setMessage(getResources().getString(R.string.message_global_sending_data));
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... paramsn) {
            Auth auth = new Auth();
            try {
                if (auth.login(user, password)) {
                    return true;
                } else {
                    error_msg = getResources().getString(R.string.message_login_invalid_credentials);
                    return false;
                }
            } catch (UserException e) {
                error_msg = e.getMessage();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
        protected Intent mServiceIntent;
        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.dismiss();
            if (result) {
                messageBox(R.string.message_login_success);
                new SyncViewTask().execute();
            } else {
                messageBox(error_msg);
            }
        }
    }

    protected class SyncViewTask extends AsyncTask<Void, Integer, Boolean> {
        protected String error_msg;

        public SyncViewTask() {

        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setTitle(R.string.app_name);
            progressDialog.setMessage(getResources().getString(R.string.message_global_sync));
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... paramsn) {
            Auth auth = new Auth();
            try {
                Sync.instance().sync();
                return true;
            } catch (UserException e) {
                error_msg = e.getMessage();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.dismiss();
            if (result) {
                // Inicia o serviço de sincronização
                if (!SyncService.RUNNING)
                    startService(new Intent(LoginActivity.this, SyncService.class));
                startActivity(MainActivity.class);
            } else {
                messageBox(error_msg);
            }
        }
    }
}
