package br.com.gabrieloliveira.saleopenerp.data;

/**
 * Created by Gabriel on 22/01/2016.
 */
public class Sync extends Base {
    public Sync() {
        TABLE_NAME = "sync";
        addFieldDefinition("id", "INTEGER PRIMARY KEY");
        addFieldDefinition("resource", "VARCHAR(64)");
        addFieldDefinition("res_id", "INTEGER NULL DEFAULT NULL");
        addFieldDefinition("local_id", "INTEGER");
        addFieldDefinition("date", "DATETIME");
        addFieldDefinition("hash", "VARCHAR(32)");
        addFieldDefinition("sync", "BOOLEAN DEFAULT '0'");
        addFieldDefinition("sync_date", "DATETIME NULL DEFAULT NULL");
        addFieldDefinition("sync_priority", "INTEGER");
        addFieldDefinition("is_create", "BOOLEAN");
        addFieldDefinition("is_delete", "BOOLEAN DEFAULT '0'");
        addFieldDefinition("sync_failed", "BOOLEAN DEFAULT '0'");
        addFieldDefinition("sync_error", "TEXT");
    }

    public void saveSync() {
        saveSync(null);
    }

    public void saveSync(Integer id) {
        set("sync", "1");
        set("sync_date", "NOW()");
        if (id != null)
            set("res_id", id.toString());
        update();
    }
}
