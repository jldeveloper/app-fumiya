package br.com.gabrieloliveira.saleopenerp.data;

import android.content.Context;
import android.support.v7.widget.GridLayout;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.gabrieloliveira.saleopenerp.SaleOpenerp;
import br.com.gabrieloliveira.saleopenerp.exception.UserException;
import br.com.gabrieloliveira.saleopenerp.lib.GPSTracker;
import br.com.gabrieloliveira.saleopenerp.lib.Tools;
import br.com.gabrieloliveira.saleopenerp.view.BaseView;

/**
 * Created by Gabriel on 22/01/2016.
 */
public class Order extends Base {
    protected String mode;
    public void setMode(String mode) {
        this.mode = mode;
        switch (this.mode) {
            case "cotation":
                clearConstraints();
                addConstraint("state", "LIKE", "draft");
                setFieldList("name", "partner_id_name", "partner_legal_name", "amount_total");
                break;
            case "comission":
                clearConstraints();
                addConstraint("comission_value", ">", "0");
                fields_list.clear();
                addFieldList("name", "partner_id_name", "date_confirm", "amount_total", "comission_value");
                setSearchType("date_range");
                FIELDS_SEARCH = new String[]{"date_confirm"};
                setListTotals("amount_total", "comission_value");
                break;
            case "orders":
                clearConstraints();
                addConstraint("state", "NOT LIKE", "draft");
                break;

        }
    }
    public Order(String mode) {
        initObject();
        setMode(mode);
    }

    public void initObject() {
        init();
        TABLE_NAME = "order";
        APP_OBJECT = "sale.order";
        FIELDS_SEARCH = new String[]{"name", "partner_id_name", "partner_legal_name"};
        addFieldDefinition("name", "VARCHAR(32)");
        addFieldDefinition("user_id", "INTEGER");
        addFieldDefinition("partner_id", "INTEGER");
        addFieldDefinition("partner_id_name", "VARCHAR(128)");
        addFieldDefinition("date_confirm", "DATETIME NULL DEFAULT NULL");
        addFieldDefinition("amount_total", "DECIMAL(12,2)");
        addFieldDefinition("comission_value", "DECIMAL(12,2)");
        addFieldDefinition("disponibilidade", "VARCHAR(56)");
        addFieldDefinition("draft_confirm", "BOOLEAN");
        addFieldDefinition("payment_term", "INTEGER");
        addFieldDefinition("state", "VARCHAR(64)");
        addFieldDefinition("date_order", "DATE");
        addFieldDefinition("sale_by_app", "BOOLEAN");
        addFieldDefinition("user_latitude", "FLOAT");
        addFieldDefinition("user_longitude", "FLOAT");
        addFieldDefinition("gen_nfe", "VARCHAR(3)");
        addFieldDefinition("partner_legal_name", "VARCHAR(128)");
        addFieldDefinition("date_sync", "DATETIME");
        addFieldDefinition("date_created_app", "DATETIME");
        EXTERNAL_FIELDS = new String[]{"name", "user_id", "partner_id", "date_confirm", "amount_total", "comission_value", "state", "disponibilidade", "payment_term", "sale_by_app", "user_latitude", "user_longitude", "draft_confirm", "gen_nfe", "date_order", "partner_legal_name", "date_sync", "date_created_app"};
        addRelation("user_id", "res.users");
        addRelation("partner_id", "res.partner");
        addRelation("payment_term", "account.payment.term");
        addFieldList("name", "partner_id_name", "partner_legal_name", "amount_total", "date_confirm");
        addLabel("name", "Pedido");
        addLabel("user_id", "Vendedor");
        addLabel("partner_id", "Cliente");
        addLabel("partner_id_name", "Nome Fantasia");
        addLabel("date_confirm", "Data Confirmação");
        addLabel("amount_total", "Total");
        addLabel("comission_value", "Comissão");
        addLabel("disponibilidade", "Disponibilidade");
        addLabel("draft_confirm", "Confirmado");
        addLabel("payment_term", "Forma de Pagamento");
        addLabel("gen_nfe", "Porcentagem Nota");
        addLabel("date_order", "Data");
        addLabel("partner_legal_name", "Razão Social");
        setListTotals("amount_total");
        setFormFields("partner_id", "draft_confirm", "payment_term", "gen_nfe");
        addFormFieldOption("gen_nfe", "0", "0%");
        addFormFieldOption("gen_nfe", "20", "20%");
        addFormFieldOption("gen_nfe", "30", "30%");
        addFormFieldOption("gen_nfe", "40", "40%");
        addFormFieldOption("gen_nfe", "50", "50%");
        addFormFieldOption("gen_nfe", "60", "60%");
        addFormFieldOption("gen_nfe", "70", "70%");
        addFormFieldOption("gen_nfe", "80", "80%");
        addFormFieldOption("gen_nfe", "90", "90%");
        addFormFieldOption("gen_nfe", "100", "100%");
        setFormFieldsRequired("partner_id", "draft_confirm", "payment_term");
    }

    public HashMap<String, Object> getExternalData() {
        HashMap<String, Object> values = super.getExternalData();
        String date_sync_value = get("date_sync");
        if (date_sync_value == null || date_sync_value.equals("") || date_sync_value.equals("0")) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            values.put("date_sync", df.format(new Date()));
        }
        return values;
    }
    public String getDefaultOrder() {
        return "date_order DESC, id DESC";
    }
    public Order() {
        initObject();
    }

    public String getListValue(String field) {
        String value = get(field);
        if (field.equals("amount_total") || field.equals("comission_value")) {
            NumberFormat formatter = NumberFormat.getCurrencyInstance();
            try {
                String moneyString = formatter.format(Double.parseDouble(value));
                return moneyString;
            } catch (NumberFormatException e) {
                return value;
            } catch (NullPointerException e) {
                return value;
            }
        }
        if (field.equals("date_confirm")) {
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(value);
                String newDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
                return newDate;
            } catch (ParseException e) {
                return value;
            } catch (NullPointerException e) {
                return value;
            }
        }
        if (field.equals("partner_id")) {
            try {
                Partner partner = new Partner();
                partner.findByRes(value);
                return partner.get("name");
            } catch (NullPointerException e) {
                return value;
            }
        }
        if (field.equals("name")) {
            try {
                if (get("draft_confirm").equals("1")) {
                    return value + " (C)";
                } else {
                    return value + " (E)";
                }
            } catch (NullPointerException e) {

            }
        }
        if (field.equals("draft_confirm")) {
            if (value.equals("1")) value = "Sim";
            else value = "Não";
        }
        return value;
    }
    public String getListTotalValue(String field, Double value) {
        if (field.equals("amount_total") || field.equals("comission_value")) {
            return Tools.getCurrencyFormat(value.toString());
        }
        return value.toString();
    }

    public Map<String, String> getFormRelationObjects() {
        Map<String, String> result = super.getFormRelationObjects();
        result.put("order_id", "sale.order.line");
        return result;
    }

    public long insert() {
        // Atribui valores padrões se o registro inserido for do tipo local
        if (isLocal()) {
            set("state", "draft");
            set("sale_by_app", "1");
            set("disponibilidade", "prontaentrega");
            // Atribui a data para ordenação
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            set("date_order", df.format(new Date()));
            SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            set("date_created_app", df2.format(new Date()));
            // Tenta pegar a latitude e longitude. O construtor já chama o método getLocation() que pega as coordenadas. Portanto, sempre que for salvar um pedido, vai demorar um tempo.
            GPSTracker gps = new GPSTracker(SaleOpenerp.getContext());
            if (gps.canGetLocation()) {
                set("user_latitude", new Double(gps.getLatitude()).toString());
                set("user_longitude", new Double(gps.getLongitude()).toString());
            }
        }
        Long id = super.insert();
        // Verifica se está inserindo um registro local. Nesse caso cria o registro para sincronia
        createSyncCreateEntry(id.toString());
        return id;
    }
    public void updateAfterRelationsSave() {
        // Calcula o total dos itens
        OrderLine line = new OrderLine();
        Double total = 0.0;
        for (Base item: line.list("order_id = ?", new String[]{get("res_id")})) {
            OrderLine order_line = (OrderLine) item;
            try {
                total += Tools.parseDouble(order_line.calcTotal());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        set("amount_total", total.toString());
        update();
    }

    protected String getAcrescimo(String acrescimo_definido, String acrescimo) {
        if (acrescimo_definido == null || acrescimo_definido.equals("0"))
            return acrescimo;
        return "0";
    }

    public List<String> getColumnsRelationTotals(List<HashMap<String, String>> data) {
        List<String> result = new ArrayList<>();
        Double subtotal = 0.0;
        Double total = 0.0;
        OrderLine line = new OrderLine();
        for (Map<String, String> item: data) {
            // Soma os valores
            try {
                subtotal += Double.parseDouble(line.calcSubtotal(item.get("price_unit"), item.get("product_uom_qty")));
                String acrescimo = getAcrescimo(item.get("acrescimo_definido"), item.get("acrescimo"));
                total += Tools.parseDouble(line.calcTotal(item.get("price_unit"), item.get("product_uom_qty"), item.get("discount_value"), acrescimo));
            } catch (NumberFormatException e) {
                // Não faz nada
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        for (String key: line.getFormFieldsList()) {
            if (key.equals("subtotal")) {
                result.add(Tools.getCurrencyFormat(subtotal));
            } else if (key.equals("total")) {
                result.add(Tools.getCurrencyFormat(total));
            } else {
                result.add("");
            }
        }
        return result;
    }

    public Integer getViewPositionRedirect() {
        if (this.mode != null && this.mode.equals("cotation"))
            return 2; // Menu cotações.
        else
            return 4; // Menu Pedidos
    }

    public Integer getFormPositionRedirect() {
        return 2;
    }

    public void addClickListener(final Context context, final GridLayout layout, final String field, final Base item, View v, final GridLayout gridSearch) {
        switch (field) {
            case "name":
                if (!mode.equals("cotation") || item.get("draft_confirm").equals("1")) {
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Se já está confirmada, então vai só abrir a view estática
                            Order order = new Order();
                            order.find(item.get("id"));
                            try {
                                BaseView.route(context, "Static", order, layout, gridSearch);
                            } catch (UserException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Order order = new Order();
                            order.find(item.get("id"));
                            try {
                                BaseView.route(context, "Form", order, layout, gridSearch);
                            } catch (UserException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                TextView txtView = (TextView) v;
                SpannableString content = new SpannableString(txtView.getText());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                txtView.setText(content);
                break;
            /*case "partner_id_name":
            case "partner_legal_name":
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Order order = new Order();
                        order.setLoaded();
                        order.set("partner_id", item.get("partner_id"));
                        try {
                            BaseView.route(context, "Form", order, layout, gridSearch);
                        } catch (UserException e) {
                            e.printStackTrace();
                        }
                    }
                });
                break;*/
        }
    }

    public ArrayList<String[]> getRelatedConstraints(String field) {
        if (field.equals("partner_id")) {
            ArrayList<String[]> result = new ArrayList<>();
            result.add(new String[]{"blocked", "=", "0"});
            return result;
        }
        return null;
    }

    public Double getAmountTotal() {
        OrderLine orderLine = new OrderLine();
        List<Base> lines = orderLine.list("order_id = ?", new String[]{get("res_id")});
        Double amount = 0.0;
        for (Base line: lines) {
            String value = line.get("total");
            if (value != null)
                amount += Tools.parseDouble(value);
        }
        return amount;
    }
    public boolean validate() {
        boolean result = super.validate();
        if (!result) return result; // Já retorna false de qualquer forma
        // Vamos executar só quando ele tiver id que é quando ele já estiver salvo
        if (get("id") != null) {
            // Pega o total e compara com o limite do cliente
            Partner partner = new Partner();
            if (partner.findByRes(get("partner_id"))) {
                Double order_total = getAmountTotal();
                Double partner_limit = Tools.parseDouble(partner.get("limit_c"));
                if (order_total > partner_limit) {
                    setValidateErrorType("custom");
                    setValidateFieldError("partner_id");
                    setValidateMessage("O valor do pedido é maior que o limite de crédito do cliente");
                    return false;
                }
            }

        }
        return true;
    }
}
