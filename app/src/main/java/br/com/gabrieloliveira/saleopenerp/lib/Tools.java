package br.com.gabrieloliveira.saleopenerp.lib;

import android.content.Context;
import android.content.Intent;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Gabriel on 21/01/2016.
 */
public class Tools {
    public static String getType(Object var) {
        return var.getClass().getName();
    }

    /**
     * Testa se o objeto é do Tipo Boolean, não testa o tipo primitivo
     * @param var
     * @return boolean
     */
    public static boolean isBoolean(Object var) {
        return is("java.lang.Boolean", var);
    }

    public static boolean isInteger(Object var) {
        return is("java.lang.Integer", var);
    }

    public static boolean isFloat(Object var) {
        return is("java.lang.Float", var);
    }

    public static boolean isDouble(Object var) {return is("java.lang.Double", var);}

    public static boolean isString(Object var) {
        return is("java.lang.String", var);
    }

    public static boolean isStringArray(Object var) { return var instanceof String[];}

    public static boolean isObjectArray(Object var) { return var instanceof Object[];}

    public static boolean is(String type, Object var) {
        return type.equals(getType(var));
    }

    public static String getString(Object var) {
        return getString(var, false);
    }

    /**
     *
     * @param var
     * @param name Indica no caso de um array se deve trazer o nome, se false, traz o id
     * @return
     */
    public static String getString(Object var, boolean name) {
        if (isBoolean(var)) {
            if ((Boolean) var) return "1";
            return "0";
        }

        if (isInteger(var)) {
            return var.toString();
        }

        if (isDouble(var)) {
            return var.toString();
        }
        if (isFloat(var)) {
            return var.toString();
        }

        if (isStringArray(var)) {
            String[] array = (String[]) var;
            if (array == null) return "";
            String result = "";
            for (int i = 0; i < array.length; i++) {
                result += array[i];
                if (i != (array.length - 1)) result += " - ";
            }
            System.out.println("Is Array");
            System.out.println(result);
            return result;
        }


        if (isString(var)) return (String)var;

        if (isObjectArray(var)) {
            Object[] value_array = (Object[]) var;
            // Nesse caso nós queremos o ID. Se no futuro for necessário podemos pegar o nome aqui que é a 2ª posição (posição 1).
            Object value;
            if (name) {
                value = value_array[1];
            } else {
                value = value_array[0];
            }
            if (isInteger(value)) {
                return value.toString();
            } else {
                return (String) value;
            }
        }
        return "";
    }

    public static String getCurrencyFormat(Double value) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        try {
            String moneyString = formatter.format(value);
            return moneyString;
        } catch (NumberFormatException e) {
            if (value == null) return "R$0,00";
            return value.toString();
        }
    }
    public static String getCurrencyFormat(String value) {
        return getCurrencyFormat(Tools.parseDouble(value));
    }

    public static void startActivity(final Context CurrentActivity, final Class TargetActivity, final HashMap<String, String> map) {
        Intent myIntent = new Intent(CurrentActivity, TargetActivity);
        if (map != null && !map.isEmpty()) {
            Iterator i = map.entrySet().iterator();
            while (i.hasNext()) {
                Map.Entry item = (Map.Entry) i.next();
                String key = (String) item.getKey();
                String value = (String) item.getValue();
                myIntent.putExtra(key, value); //Optional parameters
            }
        }
        CurrentActivity.startActivity(myIntent);
    }

    public static DecimalFormat getDecimalFormat() {
        return new DecimalFormat("0.00");
    }
    public static Double parseDouble(String value) {
        String original_value = value;
        // Verifica se contém vírgula. Se conter somente um ponto, então retorna o parseDouble
        if (value == null) return null;
        if (value.contains(".")) {
            if (value.contains(",")) {
                // Se conter vírgula é um número formatado. E como estamos no Brasil, ele formata o ponto para milhares e vírgula para decimais.
                value = value.replace(",", "|");
                value = value.replaceAll("\\.", "");
                value = value.replace("|", ".");
                value = value.replace("R$", "");
                return Double.parseDouble(value);
            } else {
                // Se só contém ponto é uma representação string de float, então é só converter
                return Double.parseDouble(value);
            }
        }
        // Verifica se contém o 'R$' e apaga
        if (value.contains("R$")) {
            value = value.replace("R$", "");
        }
        DecimalFormat df = getDecimalFormat();
        try {
            Number number = df.parse(value);
            return number.doubleValue();
        } catch (ParseException e) {
            System.out.println(original_value + " FAILED PARSE DOUBLE ##########");
            return 0.0;
        }
    }

    public static String formatDouble(Double value) {
        DecimalFormat df = getDecimalFormat();
        return df.format(value);
    }

    public static String formatDouble(String value) {
        return formatDouble(Double.parseDouble(value));
    }
}
