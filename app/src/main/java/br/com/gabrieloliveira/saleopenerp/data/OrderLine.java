package br.com.gabrieloliveira.saleopenerp.data;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import br.com.gabrieloliveira.saleopenerp.lib.Tools;

/**
 * Created by Gabriel on 03/02/2016.
 */
public class OrderLine extends Base {
    public OrderLine() {
        initObject();
    }

    public void initObject() {
        init();
        TABLE_NAME = "order_line";
        APP_OBJECT = "sale.order.line";
        DISPLAY_NAME = "Item do Pedido";
        EXTERNAL_FIELDS = new String[]{"name", "order_id", "product_id", "product_uom_qty", "product_uom", "fiscal_category_id", "fiscal_position", "price_unit", "discount", "delay"};
        FIELDS_SEARCH = new String[]{"name"};
        addFieldDefinition("name", "TEXT");
        addFieldDefinition("order_id", "Pedido de Venda");
        addFieldDefinition("product_id", "INTEGER");
        addFieldDefinition("product_uom_qty", "DECIMAL(10,3)");
        addFieldDefinition("product_uom", "INTEGER");
        addFieldDefinition("fiscal_category_id", "INTEGER");
        addFieldDefinition("fiscal_position", "INTEGER");
        addFieldDefinition("price_unit", "DECIMAL(10,2)");
        addFieldDefinition("discount", "DECIMAL(7,4)");
        addFieldDefinition("delay", "FLOAT");
        addFieldDefinition("discount_value", "DECIMAL(10,2)");
        addFieldDefinition("subtotal", "DECIMAL(10,2)");
        addFieldDefinition("total", "DECIMAL(10,2)");
        addFieldDefinition("price_unit_total", "DECIMAL(10,2)");
        addFieldDefinition("acrescimo", "DECIMAL(10,2)");
        addFieldDefinition("acrescimo_definido", "BOOLEAN DEFAULT '1'");

        addRelation("product_id", "product.product");
        addRelation("product_uom", "product.uom");
        addRelation("fiscal_category_id", "l10n_br_account.fiscal.category");
        addRelation("fiscal_position", "account.fiscal.position");

        setFormFields("product_id", "product_uom_qty", "price_unit", "discount_value", "acrescimo", "price_unit_total", "total");

        addLabel("product_id", "Produto");
        addLabel("product_uom_qty", "Quantidade");
        addLabel("price_unit", "Preço Unitário");
        addLabel("discount", "Desconto");
        addLabel("discount_value", "Valor do Desconto");
        addLabel("total", "Valor c/ Desconto");
        addLabel("subtotal", "Subtotal");
        addLabel("price_unit_total", "Total Unitário");
        addLabel("acrescimo", "Acréscimo");
        setSyncPriority(1); // O valor é ordenado do menor para o maior. Como o objeto Order possui prioridade 0, as linhas em order_line devem ser criadas depois
        addDeppend("order_id", "sale.order");
        setFormFieldsRequired("product_id", "product_uom_qty", "price_unit");
        addFormFieldUnique("product_id");
        setFormFieldsReadOnly("price_unit", "total", "price_unit_total");
        setFieldsCurrencyFormat("price_unit", "discount_value", "subtotal", "total", "price_unit_total", "acrescimo");
    }

    public ArrayList<String> getFormFieldsList() {
        ArrayList<String> list = super.getFormFieldsList();
        list.add("product_id");
        list.add("product_uom_qty");
        list.add("price_unit");
        list.add("subtotal");
        list.add("total");
        return list;
    }

    public String getListValue(String field) {
        String value = super.getListValue(field);
        if (field.equals("product_id")) {
            Product product = new Product();
            product.findByRes(value);
            return product.get("name");
        }
        if (field.equals("total")) {
            if (value == null || value.equals("")) {
                value = calcTotal(get("price_unit"), get("product_uom_qty"), get("discount_value"), "0"); // Para a lista sempre passa 0 quando ele já estiver salvo
            }
            return Tools.getCurrencyFormat(value);
        }
        if (field.equals("subtotal")) {
            if (value == null || value.equals("")) {
                value = calcSubtotal(get("price_unit"), get("product_uom_qty"));
            }
            return Tools.getCurrencyFormat(value);
        }
        if (field.equals("price_unit")) {
            return Tools.getCurrencyFormat(value);
        }
        return value;
    }

    public String calcSubtotal(String price_unit, String qty) {
        try {
            Double result = Tools.parseDouble(price_unit) * Double.parseDouble(qty);
            return result.toString();
        } catch (NumberFormatException e) {
            return "0";
        }
    }

    public String calcTotal(String price_unit, String qty, String discount_value, String acrescimo) {
        if (discount_value == null || discount_value.equals("")) discount_value = "0";
        if (acrescimo == null || acrescimo.equals("")) acrescimo = "0";
        try {
            Double middle = Tools.parseDouble(price_unit) - Tools.parseDouble(discount_value) + Tools.parseDouble(acrescimo);
            Double result = Tools.parseDouble(calcSubtotal(middle.toString(), qty));
            return result.toString();
        } catch (NumberFormatException e) {
            return "0";
        }
    }

    public String calcTotal() {
        String acrescimo = "0";
        if (get("acrescimo_definido") == null || get("acrescimo_definido").equals("0")) {
            acrescimo = get("acrescimo");
        }

        return calcTotal(get("price_unit"), get("product_uom_qty"), get("discount_value"), acrescimo);
    }

    public List<String> getOnChangeFields() {
        List<String> list = super.getOnChangeFields();
        list.add("product_id");
        list.add("discount_value");
        list.add("product_uom_qty");
        list.add("acrescimo");
        return list;
    }

    public HashMap<String, String> processOnchange(String field, String value, HashMap<String, String> values) {
        HashMap<String, String> dict = super.processOnchange(field, value, values);
        switch (field) {
            case "product_id":
                Product product = new Product();
                product.findByRes(value);
                dict.put("price_unit", Tools.formatDouble(product.get("list_price")));
            case "discount_value":
                // Pega o valor do preço unitário para formar a porcentagem
                try {
                    Double discount_value = Tools.parseDouble(values.get("discount_value")) - Tools.parseDouble(values.get("acrescimo"));
                    Double price_unit = Tools.parseDouble(values.get("price_unit"));
                    Double result = discount_value * 100 / price_unit;
                    dict.put("discount", result.toString());
                } catch (NumberFormatException e) {
                    // Não faz nada nesse caso
                }
            case "acrescimo":
            case "product_uom_qty":
                try {
                    // Muda, agora o desconto é sobre o preço unitário
                    String price_unit = values.get("price_unit");
                    if (!values.get("discount_value").equals("") || !values.get("acrescimo").equals("")) {
                        Double result = Tools.parseDouble(price_unit) - Tools.parseDouble(values.get("discount_value")) + Tools.parseDouble(values.get("acrescimo"));
                        //DecimalFormat df = new DecimalFormat("0.00");
                        price_unit = result.toString();
                    }
                    Double total = Tools.parseDouble(values.get("product_uom_qty")) * Tools.parseDouble(price_unit);
                    /*if (!values.get("discount_value").equals("")) {
                        total = total - Double.parseDouble(values.get("discount_value"));
                    }*/
                    dict.put("price_unit_total", Tools.formatDouble(price_unit));
                    //DecimalFormat df = new DecimalFormat("0.00");
                    dict.put("total", Tools.formatDouble(total.toString()));
                } catch (NumberFormatException e) {
                    // Não faz nada quando falha ao converter o double
                }
                break;
        }
        return dict;
    }

    /**
     * Atenção que esse método somente sobrescreve o método de nível mais alto que é o insert sem parâmetros. Isto porque as views somente utilizam este método.
     * @return
     */
    public long insert() {
        // Atribui o valor do desconto caso haja.
        if (isLocal()) {
            if (get("discount_value") != null && !get("discount_value").equals("")) {
                Double discount_value = Tools.parseDouble(get("discount_value"));
                Double price_unit = Tools.parseDouble(get("price_unit"));
                Double result = discount_value * 100 / price_unit;
                set("discount", result.toString());
            }
            if (get("acrescimo") != null && !get("acrescimo").equals("")) {
                Double result = Tools.parseDouble(get("acrescimo")) + Tools.parseDouble(get("price_unit"));
                set("price_unit", result.toString());
            }
        }
        Long id = super.insert();
        createSyncCreateEntry(id.toString());
        return id;
    }
}
