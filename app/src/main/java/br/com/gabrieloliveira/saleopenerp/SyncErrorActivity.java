package br.com.gabrieloliveira.saleopenerp;

import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import br.com.gabrieloliveira.saleopenerp.data.Base;
import br.com.gabrieloliveira.saleopenerp.data.Sync;
import br.com.gabrieloliveira.saleopenerp.exception.UserException;
import br.com.gabrieloliveira.saleopenerp.lib.BaseActivity;
import br.com.gabrieloliveira.saleopenerp.view.BaseView;

public class SyncErrorActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_error);
        initElements();
    }

    protected void initElements() {
        Button button = (Button) findViewById(R.id.button);
        GridLayout gridView = (GridLayout) findViewById(R.id.gridLayout);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainActivity.class);
            }
        });

        Sync sync = new Sync();
        List<Base> result = sync.list("sync_failed = ?", new String[]{"0"});
        try {
            BaseView.route(this, "List", new Sync(), gridView, null);
        } catch (UserException e) {

        }
    }
}
