package br.com.gabrieloliveira.saleopenerp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.support.v7.widget.GridLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.gabrieloliveira.saleopenerp.data.Order;
import br.com.gabrieloliveira.saleopenerp.data.Partner;
import br.com.gabrieloliveira.saleopenerp.data.Product;
import br.com.gabrieloliveira.saleopenerp.exception.UserException;
import br.com.gabrieloliveira.saleopenerp.lib.Auth;
import br.com.gabrieloliveira.saleopenerp.lib.BaseActivity;
import br.com.gabrieloliveira.saleopenerp.lib.Sync;
import br.com.gabrieloliveira.saleopenerp.view.BaseView;

public class MainActivity extends BaseActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Antes de criar verificamos se o usuário está logado
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        if (!Auth.instance().isLogged()) {
            // Muda para a outra Activity
            startActivity(LoginActivity.class);
            return;
        } else {
            if (!SyncService.RUNNING) startService(new Intent(this, SyncService.class));
        }

        Bundle extras = this.getIntent().getExtras();
        if (extras != null && extras.containsKey("initial")) {
            onNavigationDrawerItemSelected(Integer.parseInt(extras.getString("initial")));
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        GridLayout view = (GridLayout) findViewById(R.id.grd_main_main);
        GridLayout layout = (GridLayout) findViewById(R.id.grd_main_search);
        try {
            switch (number) {
                case 1: // Clientes
                    mTitle = getString(R.string.title_section1);
                    BaseView.route(this, "List", new Partner(), view, layout);
                    break;
                case 2: // Produtos
                    mTitle = getString(R.string.title_section2);
                    BaseView.route(this, "List", new Product("can_sale"), view, layout);
                    break;
                case 3: // Cotações
                    mTitle = getString(R.string.title_section3);
                    BaseView.route(this, "List", new Order("cotation"), view, layout);
                    break;
                case 4: // Adicionar cotação
                    mTitle = getString(R.string.title_section8);
                    Order order = new Order();
                    order.loaded = true;
                    order.set("gen_nfe", "30");
                    BaseView.route(this, "Form", order, view, layout);
                    break;
                case 5: // Pedidos
                    mTitle = getString(R.string.title_section4);
                    BaseView.route(this, "List", new Order("orders"), view, layout);
                    break;
                case 6: // Comissões
                    mTitle = getString(R.string.title_section5);
                    BaseView.route(this, "List", new Order("comission"), view, layout);
                    break;
                case 7: // Erros de sincronização
                    mTitle = getString(R.string.title_section9);
                    startActivity(SyncErrorActivity.class);
                    break;
                case 8:
                    // Logout
                    if (!Auth.instance().canLogout()) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                        alert.setTitle("Atenção!");
                        alert.setCancelable(true);
                        alert.setMessage("Ainda existem registros pendentes de sincronização. Se você continuar, eles serão excluídos.");
                        alert.setPositiveButton("CONTINUAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Auth.instance().logout();
                                startActivity(LoginActivity.class);
                                stopService(new Intent(MainActivity.this, SyncService.class));
                            }
                        });
                        alert.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        alert.show();
                    } else {
                        Auth.instance().logout();
                        startActivity(LoginActivity.class);
                        stopService(new Intent(this, SyncService.class));
                    }
                    break;

            }
        } catch (UserException e) {
            messageBox(e.getMessage());
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    protected ProgressDialog progressDialog;

    protected class SyncViewTask extends AsyncTask<Void, Integer, Boolean> {
        protected String error_msg;

        public SyncViewTask() {

        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle(R.string.app_name);
            progressDialog.setMessage(getResources().getString(R.string.message_global_sync));
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... paramsn) {
            try {
                Sync.instance().sync();
                return true;
            } catch (UserException e) {
                error_msg = e.getMessage();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
        @Override
        protected void onPostExecute(Boolean result) {
            try {
                progressDialog.dismiss();
                if (!result) {
                    messageBox(error_msg);
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                // Geralmente causado por troca de views, então a view não é mais válida para exibição da mensagem ou para finalizar o diálogo
            }
        }
    }

}
