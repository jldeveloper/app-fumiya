package br.com.gabrieloliveira.saleopenerp.lib;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import br.com.gabrieloliveira.saleopenerp.R;
import br.com.gabrieloliveira.saleopenerp.SaleOpenerp;

/**
 * Created by Gabriel on 23/01/2016.
 */
public class BaseActivity extends AppCompatActivity {
    protected SaleOpenerp myApp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myApp = (SaleOpenerp)this.getApplicationContext();
    }

    public void messageBox(String message) {
        messageBox(getResources().getString(R.string.app_name), message);
    }

    public void messageBox(int resource) {
        messageBox(getResources().getString(resource));
    }

    public void messageBox(String title, String message) {
        messageBox(myApp.getCurrentActivity(), title, message);
    }

    public void messageBox(Context context, String title, String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setCancelable(true);
            AlertDialog alerta = builder.create();
            alerta.show();
        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println(message);
        }
    }

    public void startActivity(final Class TargetActivity) {
        startActivity(this, TargetActivity);
    }
    public void startActivity(final Context CurrentActivity, final Class TargetActivity) {
        startActivity(CurrentActivity, TargetActivity, null);
    }

    public void startActivity(final Context CurrentActivity, final Class TargetActivity, final HashMap<String, String> map) {
        Intent myIntent = new Intent(CurrentActivity, TargetActivity);
        if (map != null && !map.isEmpty()) {
            Iterator i = map.entrySet().iterator();
            while (i.hasNext()) {
                Map.Entry item = (Map.Entry) i.next();
                String key = (String) item.getKey();
                String value = (String) item.getValue();
                myIntent.putExtra(key, value); //Optional parameters
            }
        }
        CurrentActivity.startActivity(myIntent);
    }

    protected void onResume() {
        super.onResume();
        myApp.setCurrentActivity(this);
    }
    protected void onPause() {
        clearReferences();
        super.onPause();
    }
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void clearReferences(){
        Activity currActivity = myApp.getCurrentActivity();
        if (this.equals(currActivity))
            myApp.setCurrentActivity(null);
    }
}
