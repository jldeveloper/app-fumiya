package br.com.gabrieloliveira.saleopenerp;

import android.app.IntentService;
import android.content.Intent;

import java.util.Timer;
import java.util.TimerTask;

import br.com.gabrieloliveira.saleopenerp.exception.UserException;
import br.com.gabrieloliveira.saleopenerp.lib.Sync;

/**
 * Created by Gabriel on 26/01/2016.
 */
public class SyncService extends IntentService {
    private static int SECONDS_FREQUENCY = 60;
    public SyncService() {
        super("SyncService");
    }
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public SyncService(String name) {
        super(name);
    }

    public static boolean RUNNING = false;


    @Override
    protected void onHandleIntent(Intent workIntent) {
        // Gets data from the incoming Intent
        /*String dataString = workIntent.getDataString();
        try {
            Sync.instance().sync();
        } catch (UserException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("SYNC SERVICE");
        Timer timer = new Timer();
        TimerTask tt = new TimerTask(){
            @Override
            public void run() {
                try {
                    System.out.println("######################### SYNCING ##############################");
                    Sync.instance().sync();
                } catch (UserException e) {
                    e.printStackTrace();
                }
            }
        };
        RUNNING = true;
        timer.schedule(tt,0,SECONDS_FREQUENCY * 1000); // Executa a frequencia de segundos. O número é sempre em milisegundos, por isso a multiplicação
        //return START_STICKY;
        return super.onStartCommand(intent, flags, startId);
    }
}
