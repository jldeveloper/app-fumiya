package br.com.gabrieloliveira.saleopenerp.data;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Gabriel on 20/01/2016.
 */
public class User extends Base {
    public User() {
        init();
        this.TABLE_NAME = "user";
        addFieldDefinition("name", "VARCHAR(128)");
        addFieldDefinition("login", "VARCHAR(64)");
        addFieldDefinition("company_id", "INTEGER");
        addFieldDefinition("password", "VARCHAR(64)");
        addFieldDefinition("logged", "BOOLEAN DEFAULT '1'");
        EXTERNAL_FIELDS = new String[]{"name", "login", "company_id"};
        this.APP_OBJECT = "res.users";
        this.DISPLAY_NAME = "Usuários";
        this.INCLUDE_SEARCH = false;
    }

    public static User getUser() {
        User user = new User();
        List<Base> users = user.list();
        if (users.isEmpty()) return null;
        return (User) users.get(0);
    }
}
