package br.com.gabrieloliveira.saleopenerp.exception;

/**
 * Created by Gabriel on 20/01/2016.
 */
public class UserException extends Exception {
    public UserException(String message) {
        super(message);
    }
}
