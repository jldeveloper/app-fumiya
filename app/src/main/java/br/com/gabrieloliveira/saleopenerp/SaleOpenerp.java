package br.com.gabrieloliveira.saleopenerp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

/**
 * Created by Gabriel on 20/01/2016.
 */
public class SaleOpenerp extends Application {
    private static SaleOpenerp instance;

    public SaleOpenerp() {
        instance = this;
    }

    /*public static SaleOpenerp instance() {
        if (instance == null) instance = new SaleOpenerp();
        return instance;
    }*/

    public static Context getContext() {
        return instance;
    }

    private Activity mCurrentActivity = null;
    public Activity getCurrentActivity() {
        return mCurrentActivity;
    }
    public void setCurrentActivity(Activity mCurrentActivity){
        this.mCurrentActivity = mCurrentActivity;
    }
}
