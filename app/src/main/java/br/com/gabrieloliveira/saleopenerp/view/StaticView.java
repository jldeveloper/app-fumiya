package br.com.gabrieloliveira.saleopenerp.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.widget.GridLayout;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import br.com.gabrieloliveira.saleopenerp.MainActivity;
import br.com.gabrieloliveira.saleopenerp.data.Base;
import br.com.gabrieloliveira.saleopenerp.exception.UserException;
import br.com.gabrieloliveira.saleopenerp.lib.Draw;
import br.com.gabrieloliveira.saleopenerp.lib.MoneyTextWatcher;
import br.com.gabrieloliveira.saleopenerp.lib.Tools;

/**
 * Created by Gabriel on 26/01/2016.
 */
public class StaticView extends BaseView {
    /**
     * Sempre possui um id para carregar os itens
     */
    protected String id;
    /**
     * Contém o layout principal onde é carregado o formulário
     */
    protected GridLayout view;
    /**
     * Esse mapa contém os VALORES das relações. A chave de acesso é o nome da relação retornado no Mapa getFormRelations() do objeto data.Base
     * Dentro de cada relação podem existir n Mapas do tipo chave -> valor, que são respectivamente o nome do campo apontando para o seu valor.
     * Esses valores serão depois salvos no banco de dados
     */
    protected HashMap<String, ArrayList<HashMap<String, String>>> relations_values = new HashMap<>();

    /**
     * Aponta para o Objeto StaticView que é o pai desse, ou seja, o que abriu um Dialog contendo o FormView corrente.
     * Esse atributo é usado para salvar os valores das relações
     */
    protected StaticView parent;
    /**
     * Armazena o nome da relação que é sempre o nome do campo relacionado
     */
    protected String relation_name;
    /**
     * Armazena o Dialog que é o container. No caso de FormView relacionado, para que seja possível fechá-lo ao clicar em salvar
     */
    protected Dialog dialog;

    /**
     * Armazena os containers da relação. A chave é o nome do campo relacionado vindo de getFormRelations() do @object
     * Esses objetos GridLayout são os que contém os registros das relações já adicionadas. São para exibir esses registros
     */
    protected HashMap<String, GridLayout> relations_container_list = new HashMap<>();

    public void addValueInRelation(String relation_name, HashMap<String, String> values) {
        if (relations_values.get(relation_name) == null) relations_values.put(relation_name, new ArrayList<HashMap<String, String>>());
        relations_values.get(relation_name).add(values);
    }

    public ArrayList<HashMap<String, String>> getValuesFromRelation(String relation_name) {
        return relations_values.get(relation_name);
    }
    public void setParent(StaticView parent, String relation_name, Dialog dialog) {
        this.parent = parent;
        this.relation_name = relation_name;
        this.dialog = dialog;
    }

    public void show(GridLayout view, String id) {
        this.view = view;
        this.id = id;
        object.find(id);
        show(view);
    }
    public void show(GridLayout view) {
        if (view == null) return;
        if (searchContainer != null) {
            searchContainer.removeAllViews();
        }
        this.view = view;
        if (object.get("id") != null && object.loaded) {
            id = object.get("id");
            // Carrega as relações
            Map<String, String> relations = object.getFormRelationObjects();
            Iterator iterator = relations.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = (Map.Entry) iterator.next();
                Base item = Base.getObjectByAppName(entry.getValue());
                List<Base> itens = item.list(entry.getKey() + " = ?", new String[]{object.get("res_id")});
                for (Base o: itens) {
                    addValueInRelation(entry.getKey(), (HashMap) o.getData());
                }
            }
        }
        view.removeAllViews();
        ArrayList<String> form_fields = object.getFormFields();
        // Define o row count. O formulário sempre usa 2 colunas
        // É igual ao número de campos do formulário mais 2 de cada relação e mais 1 do botão Salvar
        view.setRowCount(form_fields.size() + (2 * object.getRelationsSize()) + 1);
        view.setColumnCount(2);
        List<String> fields_onchange = object.getOnChangeFields();
        int row = 0;
        int col = 0;
        String final_value = "";
        for (final String field: form_fields) {
            String type = object.getFieldType(field);
            Draw.addViewInGrid(view, Draw.getTextView(context, object.getLabel(field), Gravity.LEFT, Gravity.CENTER), row, col);
            col++;
            switch (type) {
                case "INTEGER":
                    if (object.hasRelation(field)) {
                        // Nesse caso cria uma lista
                        final Spinner spinner = new Spinner(context);
                        Base relationObject = object.getRelationObject(field);
                        if (relationObject != null) {
                            relationObject.findByRes(object.get(field));
                            // Verifica se há constraints
                            final_value = relationObject.getRelationListNameValue();
                        }
                        //Draw.addViewInGrid(view, spinner, row, col);
                        addElement(view, spinner, row, col);
                    } else {
                        final_value = object.getListValue(field);
                    }
                    break;
                case "VARCHAR":
                case "TEXT":
                case "CHAR":
                    if (object.hasFormFieldOptions(field)) {
                        HashMap<String, String> options = object.getFormFieldOptions(field);
                        final_value = options.get(object.get(field));
                    } else {
                        final_value = object.getListValue(field);
                    }
                    break;
                case "DECIMAL":
                case "FLOAT":
                case "DOUBLE":
                    final_value = object.getListValue(field);
                    break;
                case "BOOLEAN":
                    final_value = object.getListValue(field);
                    break;
            }
            addElement(view, Draw.getTextView(context, final_value), row, col);
            row++;
            col = 0;
        }
        AtomicInteger rowa = new AtomicInteger(row);
        AtomicInteger cola = new AtomicInteger(col);
        addViewRelated(rowa, cola);
        addSaveButton(rowa, cola);
        if (object.loaded) {
            refreshRelations();
        }
    }

    public void addSaveButton(AtomicInteger row, AtomicInteger col) {
        Button button = new Button(context);
        button.setText("Voltar");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer position = object.getViewPositionRedirect();
                HashMap<String, String> map = null;
                if (position != null) {
                    map = new HashMap<String, String>();
                    map.put("initial", position.toString());
                }
                Tools.startActivity(context, MainActivity.class, map);
            }
        });

        //Draw.addViewInGrid(view, button, Draw.gridLayoutParamsColSpan(row.getAndIncrement(), col.get(), 2));
        addElement(view, button, row.getAndIncrement(), 2, col.get());
    }


    public void refreshRelations() {
        Iterator iterator = relations_container_list.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, GridLayout> entry = (Map.Entry) iterator.next();
            final String key = entry.getKey();
            if (relations_values.get(key) == null) continue;
            GridLayout view = entry.getValue();
            // Limpa a view e adiciona novamente
            view.removeAllViews();
            Base object = Base.getObjectByAppName(this.object.getFormRelationObjects().get(key));
            ArrayList<String> fields = object.getFormFieldsList();
            int row = 0;
            int col = 0;
            for (String field: fields) {
                // Adiciona o header
                addElement(view, object.getLabel(field), row, col);
                col++;
            }
            row++;
            col = 0;
            for (final HashMap<String, String> value: relations_values.get(key)) {
                for (String field: fields) {
                    object.set(field, value.get(field)); // Informa todos no objeto para que ele calcule o valor de getListValue()
                }
                for (String field: fields) {
                    addElement(view, object.getListValue(field), row, col);
                    col++;
                }
                row++;
                col = 0;
            }
            // Agora adiciona as colunas dos totais
            List<String> values = this.object.getColumnsRelationTotals(relations_values.get(key));
            if (values != null) {
                for (String value: values) {
                    addElement(view, value, row, col);
                    col++;
                }
                row++;
                col = 0;
            }
        }
    }

    public View addElement(GridLayout view, String text, int line, int column) {
        String[] colors = new String[]{"#CCCCCC", "#FFFFFF"};
        String color = colors[line % 2];
        TextView txt = Draw.getTextView(this.context, text);
        txt.setHorizontallyScrolling(false);
        GridLayout.LayoutParams params = Draw.gridLayoutParams(line, column);
        params.width = 0;
        txt.setBackgroundColor(Color.parseColor(color));
        txt.getLayoutParams().width = 0;
        txt.setPadding(10, 10, 10, 10);
        txt.setClickable(true);
        Draw.addViewInGrid(view, txt, params);
        return txt;
        //view.requestFocus();
    }

    public void addElement(GridLayout view, View object, int line, int column) {
        GridLayout.LayoutParams params = Draw.gridLayoutParams(line, column);
        addElement(view, object, params);
    }

    public void addElement(GridLayout view, View object, GridLayout.LayoutParams params) {
        params.width = 0;
        object.setPadding(10, 10, 10, 10);
        object.setClickable(true);
        Draw.addViewInGrid(view, object, params);
        if (object.getLayoutParams() != null) {
            object.getLayoutParams().width = GridLayout.LayoutParams.MATCH_PARENT;
        }
    }

    public void addElement(GridLayout view, View object, int line, int colspan, int start) {
        GridLayout.LayoutParams params = Draw.gridLayoutParamsColSpan(line, start, colspan);
        addElement(view, object, params);
    }

    public void addViewRelated(AtomicInteger row, AtomicInteger column) {
        Map<String, String> objects = this.object.getFormRelationObjects();
        if (objects.isEmpty()) return;
        // Vamos adicionar botões para cada número de objetos
        Iterator iterator = objects.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry) iterator.next();
            final String key = entry.getKey();
            String value = entry.getValue();
            final Base item = Base.getObjectByAppName(value);
            GridLayout containerList = new GridLayout(context);
            relations_container_list.put(key, containerList);
            //Draw.addViewInGrid(this.view, containerList, Draw.gridLayoutParamsColSpan(row.getAndIncrement(), 2, column.get()));
            addElement(view, containerList, row.getAndIncrement(), 2, column.get());
        }
    }
}
