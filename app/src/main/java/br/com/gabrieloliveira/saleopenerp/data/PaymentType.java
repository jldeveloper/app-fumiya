package br.com.gabrieloliveira.saleopenerp.data;

/**
 * Created by Gabriel on 03/02/2016.
 */
public class PaymentType extends Base {
    public PaymentType() {
        initObject();
    }

    public void initObject() {
        init();
        TABLE_NAME = "payment_type";
        APP_OBJECT = "payment.type";
        FIELDS_SEARCH = new String[]{"name"};
        addFieldDefinition("name", "VARCHAR(64)");
        addFieldDefinition("active", "BOOLEAN");
        addFieldDefinition("code", "VARCHAR(64)");
        addFieldDefinition("company_id", "INTEGER");
        EXTERNAL_FIELDS = new String[]{"name", "active", "code", "company_id"};
        addLabel("name", "Nome");
        addLabel("active", "Ativo");
        addLabel("code", "Código");
        addLabel("company_id", "Empresa");

    }
}
