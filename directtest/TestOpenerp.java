import java.net.MalformedURLException;
import java.net.URL;

import de.timroes.axmlrpc.XMLRPCClient;
import de.timroes.axmlrpc.XMLRPCException;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
class TestOpenerp {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        System.out.println(args.getClass().getName());
        instance().password = args[1];
        instance().login(args[0], args[1]);
        instance().list();
        //instance().create();
        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(8);
        instance().update(ids);
    }
    
    public static TestOpenerp instance;
    public static TestOpenerp instance() {
        if (instance == null)
            instance = new TestOpenerp();
        return instance;
    }
    protected XMLRPCClient client;
    protected String database = "erpestribos";
    protected String password;
    protected Integer uid;

    public void connect(String type) {
        String action;
        if (type == "login") {
            action = "/xmlrpc/common";
        } else {
            action = "/xmlrpc/object";
        }
        try {
            client = new XMLRPCClient(new URL("http", "localhost", 8080, action));
        } catch (MalformedURLException e) {
            // pass like Python :)
        }
    }
    
    public void connectLogin() {
        connect("login");
    }
    
    public void connectObject() {
        connect("object");
    }


    public boolean login(String username, String password) {
        connectLogin();
        // Esse método de login necessita que o usuário possua conexão. Por isso testa e retorna uma exceção caso nao esteja
        //checkConnected();
        try {
            Object o = client.call("login", database, username, password);
            if (o.getClass().getName() == "java.lang.Integer") {
                uid = (Integer) o;
            } else {
                System.out.println("Usuário ou senha incorretos");
            }
            System.out.println(o.getClass().getName());
            // Agora vamos pegar uma lista
        } catch (XMLRPCException e) {
            e.printStackTrace();
        }
        return true;
    }
    
    @SuppressWarnings("unchecked")
    public void list() {
        connectObject();
        if (uid != null) {
            try {
                Object[] o = (Object[]) client.call("execute", database, uid, password, "sale.order", "search", new ArrayList<String>());
                ArrayList<Integer> ids = new ArrayList<>();
                for (int i = 0; i < o.length; i++) {
                    ids.add((Integer)o[i]);
                }
                get(ids);
                System.out.println(o);
            } catch (XMLRPCException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public void get(ArrayList<Integer> ids) {
        connectObject();
        try {
            ArrayList<String> fields = new ArrayList<String>();
            fields.add("name");
            fields.add("partner_id");
            Object[] o = (Object[]) client.call("execute", database, uid, password, "sale.order", "read", ids, fields);
            for (int i = 0; i < o.length; i++) {
                HashMap<String, Object> map = (HashMap<String, Object>) o[i];
                System.out.println(map.get("name"));
                System.out.println(map.get("partner_id"));
            }
            System.out.println(o);
        } catch (XMLRPCException e) {
            e.printStackTrace();
        }
    }
    
    public void create() {
        connectObject();
        try {
            HashMap<String, String> data = new HashMap<>();
            data.put("name", "Gabriel From Java");
            data.put("lang", "pt_BR");
            data.put("phone", "92264703");
            data.put("limit_c", "123.20");
            data.put("customer", "True");
            System.out.println(client.call("execute", database, uid, password, "res.partner", "create", data));
            
        } catch (XMLRPCException e) {
            e.printStackTrace();
        }
    }
    
    public void update(ArrayList<Integer> ids) {
        connectObject();
        try {
            HashMap<String, String> data = new HashMap<>();
            data.put("name", "Gabriel UPDATE From Java");
            System.out.println(client.call("execute", database, uid, password, "res.partner", "write", ids, data));
        } catch (XMLRPCException e) {
            e.printStackTrace();
        }
    }
    
}
    /*
    NOTAS:
    Todas as chamadas podem retornar um objeto do tipo Boolean. Por isso testar a resposta com o tipo de objeto para verificar se é um boolean.
    O método update sempre retorna boolean.
    Método create retorna um Objeto do tipo Integer
    Método read retorna um Array do tipo Object[] que contém tipos Map<String, Object> com a chave dos campos enviados e o seu valor como Object. É preciso saber que tipo de valor eles possuem 
    para fazer o casting corretamente do valor.
    Método search retorna um array de objetos do tipo Object[] contendo um valor Integer em cada um.
    Login pode retornar um tipo Integer caso os dados sejam válidos, ou um Boolean com false caso seja inválido ou ocorra algum erro
    */